unit InformObjConst;

interface

type
 // ASDU types (TypeId)
 AsduTypes_E = (
  NOT_DEFINITION_TPS = 0,

  M_SP_NA_1 = 1,    // single-point information
  M_SP_TA_1 = 2,    // single-point information with time tag
  M_DP_NA_1 = 3,    // double-point information
  M_DP_TA_1 = 4,    // double-point information with time tag
  M_ST_NA_1 = 5,    // step position information
  M_ST_TA_1 = 6,    // step position information with time tag
  M_BO_NA_1 = 7,    // bitstring of 32 bits
  M_BO_TA_1 = 8,    // bitstring of 32 bits with time tag
  M_ME_NA_1 = 9,    // measured value, normalized value
  M_ME_TA_1 = 10,   // measured value, normalized value with time tag
  M_ME_NB_1 = 11,   // measured value, scaled value
  M_ME_TB_1 = 12,   // measured value, scaled value with time tag
  M_ME_NC_1 = 13,   // measured value, short floating point number
  M_ME_TC_1 = 14,   // measured value, short floating point number with time tag
  M_IT_NA_1 = 15,   // integrated totals
  M_IT_TA_1 = 16,   // integrated totals with time tag
  M_EP_TA_1 = 17,   // information about using the relay protection with time tag
  M_EP_TB_1 = 18,   // packed with information about fires starting protection authorities with time tag
  M_EP_TC_1 = 19,   // packed with information about the weekend triggered circuit protection with time tag
  M_PS_NA_1 = 20,   // packed single-point information with status change detection
  M_ME_ND_1 = 21,   // measured value, normalized value without quality descriptor
  M_SP_TB_1 = 30,   // single-point information with time tag CP56Time2a
  M_DP_TB_1 = 31,   // double-point information with time tag CP56Time2a
  M_ST_TB_1 = 32,   // step position information with time tag CP56Time2a
  M_BO_TB_1 = 33,   // bitstring of 32 bit with time tag CP56Time2a
  M_ME_TD_1 = 34,   // measured value, normalized value with time tag CP56Time2a
  M_ME_TE_1 = 35,   // measured value, scaled value with time tag CP56Time2a
  M_ME_TF_1 = 36,   // measured value, short floating point number with time tag CP56Time2a
  M_IT_TB_1 = 37,   // integrated totals with time tag CP56Time2a
  M_EP_TD_1 = 38,   // event of protection equipment with time tag CP56Time2a
  M_EP_TE_1 = 39,   // packed start events of protection equipment with time tag CP56Time2a
  M_EP_TF_1 = 40,   // packed output circuit information of protection equipment with time tag CP56Time2a
  C_SC_NA_1 = 45,   // single command
  C_DC_NA_1 = 46,   // double command
  C_RC_NA_1 = 47,   // regulating step command
  C_SE_NA_1 = 48,   // set point command, normalized value
  C_SE_NB_1 = 49,   // set point command, scaled value
  C_SE_NC_1 = 50,   // set point command, short floating point number
  C_BO_NA_1 = 51,   // bitstring of 32 bits
  C_SC_TA_1 = 58,   // single command with time tag CP56Time2a
  C_DC_TA_1 = 59,   // double command with time tag CP56Time2a
  C_RC_TA_1 = 60,   // regulating step command with time tag CP56Time2a
  C_SE_TA_1 = 61,   // set point command, normalized value with time tag CP56Time2a
  C_SE_TB_1 = 62,   // set point command, scaled value with time tag CP56Time2a
  C_SE_TC_1 = 63,   // set point command, short floating-point number with time tag CP56Time2a
  C_BO_TA_1 = 64,   // bitstring of 32 bits with time tag CP56Time2a
  M_EI_NA_1 = 70,   // end of initialization
  C_IC_NA_1 = 100,  // interrogation command
  C_CI_NA_1 = 101,  // counter interrogation command
  C_RD_NA_1 = 102,  // read command
  C_CS_NA_1 = 103,  // clock synchronization command
  C_TS_NA_1 = 104,  // test command
  C_RP_NA_1 = 105,  // reset process command
  C_CD_NA_1 = 106,  // search delay command
  C_TS_TA_1 = 107,  // test command with time tag CP56Time2a
  P_ME_NA_1 = 110,  // parameter of measured value, normalized value
  P_ME_NB_1 = 111,  // parameter of measured value, scaled value
  P_ME_NC_1 = 112,  // parameter of measured value, short floating-point number
  P_AC_NA_1 = 113,  // parameter activation
  F_FR_NA_1 = 120,  // file ready
  F_SR_NA_1 = 121,  // section ready
  F_SC_NA_1 = 122,  // call directory, select file, call file, call section
  F_LS_NA_1 = 123,  // last section, last segment
  F_AF_NA_1 = 124,  // ack file, ack section
  F_SG_NA_1 = 125,  // segment
  F_DR_TA_1 = 126,  // directory
  F_SC_NB_1 = 127   // Query Log - Request archive file
 );

 // Cause of Transmision (CauseTx)
 CauseOfTrans_E = (
  NOT_DEFINITION_CS = 0,

  PER_CYC         = 1,
  BACK            = 2,
  SPONT           = 3,
  INIT            = 4,
  REQ             = 5,
  ACT             = 6,
  ACT_CON         = 7,
  DEACT           = 8,
  DEAC_TCON       = 9,
  ACT_TERM        = 10,
  RETERM          = 11,
  RETLOC          = 12,
  FILE_TRANS      = 13,
  INROGEN         = 20,
  INRO1           = 21,
  INRO2           = 22,
  INRO3           = 23,
  INRO4           = 24,
  INRO5           = 25,
  INRO6           = 26,
  INRO7           = 27,
  INRO8           = 28,
  INRO9           = 29,
  INRO10          = 30,
  INRO11          = 31,
  INRO12          = 32,
  INRO13          = 33,
  INRO14          = 34,
  INRO15          = 35,
  INRO16          = 36,
  REQCOGEN        = 37,
  REQCO1          = 38,
  REQCO2          = 39,
  REQCO3          = 40,
  REQCO4          = 41,
  UK_TYPE_ID      = 44,
  UK_CAUSE_TX     = 45,
  UK_COM_ADR_ASDU = 46,
  UK_IOA          = 47
 );

implementation

end.

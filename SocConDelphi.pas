unit SocConDelphi;

interface
uses   classes, Sockets, SyncObjs, IecSerializer;

const
  ConnectionsCount = 255;

type

  IecMaster = class(TThread)
    tcp : TTcpClient;
    address : string;
    port : string;
    IsRun : Boolean;
    IsConnected : Boolean;
    OnReceive : procedure(buf : ApduBuffer_T; id : Integer) of object;


    procedure Execute(); override;
    procedure Connect();
    procedure Disonnect();
    procedure Send(buf : ApduBuffer_T; Len : Integer);
    procedure OnError(sender : TObject; SocketError : Integer);
  end;

  Connection_T = record
    client : TCustomIpClient;
    IsActive : Boolean;
  end;

  IecSlave = class(TThread)
    tcp : TTcpServer;
    address : string;
    port : string;
    IsRun : Boolean;
    OnReceive : procedure(buf : ApduBuffer_T; id : Integer) of object;
    ActiveCon : array[0..ConnectionsCount] of Connection_T;
    CS: TCriticalSection;
    connn : TCustomIpClient;

    procedure Execute(); override;
    procedure Listen();
    procedure Close(id : Integer);
    procedure Send(buf : ApduBuffer_T; id, len : Integer);
    procedure TcpServersAccept(Sender: TObject; ClientSocket: TCustomIpClient);
    function ConIsAtive(id : Integer) : Boolean;
    function RegConnection(cli : TCustomIpClient) : Integer;
    procedure UnregConnection(id : Integer);
    procedure OnError(sender : TObject; SocketError : Integer);
  end;

  function ConfirmBuffs(buf1, buf2 : ApduBuffer_T) : Boolean;

implementation
//******************************************************************************
//**********************   IecMaster methods   *********************************
//******************************************************************************
  procedure IecMaster.Connect();
  begin
    //IsRun := True;
    IsConnected := True;
  end;

  procedure IecMaster.Disonnect();
  begin
    IsConnected := False;
    tcp.Disconnect;
    tcp.Close;
  end;

  procedure IecMaster.Execute();
  var
    buf : ApduBuffer_T;
  begin
    tcp := TTcpClient.Create(nil);
    tcp.RemoteHost := address;
    tcp.RemotePort := port;
    tcp.OnError := OnError;

    while true do
    begin
      if tcp.Connected then
      begin
        tcp.ReceiveBuf(buf, sizeof(buf));
        if tcp.BytesReceived > 0 then OnReceive(buf, -1);
      end
      else if IsConnected then
      begin
        tcp.Connect;
      end
      else sleep(300);
      //write('.');
    end;
  end;

  procedure IecMaster.Send(buf : ApduBuffer_T; Len : Integer);
  begin
    if tcp = nil then Exit;
    while not tcp.Connected do
    begin
      //write('!');
    end;
    tcp.SendBuf(buf, Len);
  end;

  procedure IecMaster.OnError(sender : TObject; SocketError : Integer);
  begin
    tcp.Close;
    tcp.Disconnect;
  end;

//******************************************************************************
//**********************   IecSlave methods   **********************************
//******************************************************************************
  procedure IecSlave.Listen();
  begin
    //IsRun := True;
  end;

  procedure IecSlave.Close(id : Integer);
  begin
    UnregConnection(id);
  end;

  procedure IecSlave.Execute();
  var
    buf : ApduBuffer_T;
  begin
    CS := TCriticalSection.Create;

    tcp := TTcpServer.Create(nil);
    tcp.LocalHost := '';
    tcp.LocalPort := port;
    tcp.BlockMode := bmThreadBlocking;
    //tcp.BlockMode := bmNonBlocking;
    tcp.OnAccept := TcpServersAccept;
    tcp.OnError := OnError;
    tcp.Active := True;

    while true do
    begin
      //writeln('suke');
      tcp.WaitForConnection;
      sleep(300);
    end;
  end;

  procedure IecSlave.Send(buf : ApduBuffer_T; id, len : Integer);
  begin
    if tcp = nil then Exit;
    ActiveCon[id].client.SendBuf(buf, sizeof(buf))
    //tcp.SendBuf(buf, sizeof(buf));
  end;

  procedure IecSlave.TcpServersAccept(Sender: TObject; ClientSocket: TCustomIpClient);
  var
    buf : ApduBuffer_T;
    loop : Boolean;
    id : Integer;
  begin
    ClientSocket.OnError := OnError;
    connn := ClientSocket;
    id := RegConnection(ClientSocket);
    //writeln('open ', id);
    while ConIsAtive(id) and ClientSocket.Connected do
    begin
      ClientSocket.ReceiveBuf(buf, sizeof(buf));
      ////writeln('##################');
      OnReceive(buf, id);

    end;
    UnregConnection(id);
    ClientSocket.Close;
    ClientSocket.Disconnect;
    //writeln('close ', id);
  end;

  procedure IecSlave.OnError(sender : TObject; SocketError : Integer);
  begin
    //(sender as TTcpServer).Close;
    (sender as TCustomIpClient).Close;
    (sender as TCustomIpClient).Disconnect;
  end;


  function IecSlave.ConIsAtive(id : Integer) : Boolean;
  begin
    CS.Enter;
    Result := ActiveCon[id].IsActive and (not (ActiveCon[id].client = nil));
    CS.Leave;
  end;

  function IecSlave.RegConnection(cli : TCustomIpClient) : Integer;
  var
    id : Integer;
  begin
    CS.Enter;
    for id := 0 to ConnectionsCount do
    begin
      if not ActiveCon[id].IsActive then
      begin
        Result := id;
        ActiveCon[id].IsActive := True;
        ActiveCon[id].client := cli;
        CS.Leave;
        Exit;
      end;
    end;
    Result := -1;
    CS.Leave;
  end;

  procedure IecSlave.UnregConnection(id : Integer);
  begin
    CS.Enter;
    ActiveCon[id].IsActive := False;
    ActiveCon[id].client := nil;
    CS.Leave;
  end;


  function ConfirmBuffs(buf1, buf2 : ApduBuffer_T) : Boolean;
  var
    i : integer;
  begin
    for i := 0 to Length(buf1) - 1 do
    begin
      if not(buf1[i] = buf2[i]) then
      begin
        Result := False;
        Exit;
      end;
    end;
    Result := True;
  end;
end.

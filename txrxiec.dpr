
program txrxiec;

{$APPTYPE CONSOLE}



uses
  {$IFDEF UNIX} cthreads, {$ENDIF}
  SysUtils,
  IecSerializer in 'IecSerializer.pas',
  ASDU in 'ASDU.pas',
  Tools in 'Tools.pas',
  ASDUData in 'ASDUData.pas',
  InformObj in 'InformObj.pas',
  InformObjFields in 'InformObjFields.pas',
  InformObjConst in 'InformObjConst.pas',
  InformObjStructures in 'InformObjStructures.pas',
  Comunicator in 'Comunicator.pas',
  SocConFPC in 'SocConFPC.pas';


const
  TERMINAL_STR_LEN = 30;
var
  Iec104Ser_obj : Iec104Ser;
  APDU_Dat:  ApduBuffer_T;
  descr : AsduDescr;
  i : Integer;
  master : IecMaster;
  slave : IecSlave;
  IEC104_obj : IEC104;
  //soc : TSocketConnection ;




procedure Uhandler(operation : UCommands);
begin
  writeln('U_HANDLER:');

  case operation  of
    UCommands.U_STARTDT_ACT :
    begin
      writeln(' operation = STARTDT_ACT');
    end;

    UCommands.U_STARTDT_CON :
    begin
      writeln(' operation = U_STARTDT_CON');
    end;

    UCommands.U_STOPDT_ACT :
    begin
      writeln(' operation = U_STOPDT_ACT');
    end;

    UCommands.U_STOPDT_CON :
    begin
      writeln(' operation = U_STOPDT_CON');
    end;

    UCommands.U_TESTFR_ACT :
    begin
      writeln(' operation = U_TESTFR_ACT');
    end;

    UCommands.U_TESTFR_CON :
    begin
      writeln(' operation = U_TESTFR_CON');
    end;
  end;
end;


procedure ValueHandler(descr : AsduDescr; id : Integer);
var
  i : Integer;
begin
  writeln('type = ', Integer(descr.ASDUType));
  for i := 0 to descr.StrcQual.VSQ do
  begin
    writeln('address = ', descr.InfObjDescr[i].InfObjAddress, '; value = ', descr.InfObjDescr[i].CSENC1.R32IEEE754.Value:0:5);
  end;
end;

procedure ReceiveHandler(buf : ApduBuffer_T);
begin
  PrintAPDU(buf);
end;

procedure ReceiveHandler2(buf : ApduBuffer_T);
begin
  PrintAPDU(buf);
  //writeln('s');
end;


begin
  try
    {Iec104Ser_obj := Iec104Ser.Create;
    //Iec104Ser_obj.U_Handler := Uhandler;
    //Iec104Ser_obj.I_Handler := Ihandler;
    //Iec104Ser_obj.GenerateU(UCommands.U_STARTDT_ACT);}

    descr.ASDUType := ASDUTypes.C_SE_NC_1;
    descr.StrcQual.VSQ := 10;
    //descr.StrcQual.SQ := false;
    descr.StrcQual.SQ := true;
    descr.CauseTrans.CauseTrans := CauseOfTrans.PER_CYC;
    descr.CauseTrans.PN := True;
    descr.CauseTrans.T := True;
    descr.OriginatorAdderess := 12;
    descr.ASDUAddress := $cccc;
    descr.InfObjAddress := $ffffff;
    //descr.InfObjAddress := 111;

    for i := 0 to descr.StrcQual.VSQ - 1 do
    begin
    descr.InfObjDescr[i].InfObjAddress := i;
    descr.InfObjDescr[i].CSENC1.R32IEEE754.Value := 11.33;
    descr.InfObjDescr[i].CSENC1.QOS.QL := 1;
    end;



    IEC104_obj := IEC104.Create(Direction_T.MASTER, '127.0.0.1', '2404');
    IEC104_obj.UserValueHandler := ValueHandler;
    IEC104_obj.PushSingle(descr);
    //IEC104_obj.AddItem(descr);
    //IEC104_obj.Push();
    readln;


  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.

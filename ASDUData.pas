unit ASDUData;

interface
  uses Tools, InformObjFields, InformObjConst, InformObjStructures;

  const
    ASDU_MIN_LEN               = 0;
    ASDU_MAX_LEN               = 249;
    INFORMATION_OBJECT_MAX_LEN = ASDU_MAX_LEN - 6;
    VSQ_MAX                    = 127;


  type
    // data type
    ASDU_T              = array[0..ASDU_MAX_LEN] of byte;
    ObjectInformation_T = array[0..INFORMATION_OBJECT_MAX_LEN] of byte;

    // addresation
    // The header ASDU contains a fieds variable lengt
    // so using enumeration shuld be only with the GetAddrOffset() function
    // for the geting actual address shift in array
    Addr_E = (
      TYPE_IDENTIFICATION        = 0,
      VARIABLE_STRCTRE_QUALIFIER = 1,
      CAUSE_OF_TRANSMISSION      = 2,
      ORIGINATOR_ADDRESS         = 3, // optional
      ASDU_ADDRESS_PATH1         = 4,
      ASDU_ADDRESS_PATH2         = 5, // optional
      OBJECT_INFARMATION         = 6
    );

//******************************************************************************
//******************************************************************************
//******************************************************************************

    Iec104_R = record
      SequenceNumberS : Integer;
      SequenceNumberR : Integer;
    end;

    StructureQualifier_R = record
      VSQ : 0..VSQ_MAX;
      SQ  : Boolean;
    end;

    CauseOfTransmittion_R = record
      CauseTrans : CauseOfTrans_E;
      PN         : Boolean;
      T          : Boolean;
    end;


    InfObjDescr_R = record
      InfObjAddress   : Integer; // used if  StrcQual.SQ = 0

    case AsduType : AsduTypes_E of

      M_SP_NA_1 : (MSPNA1 : M_SP_NA_1_R);
      M_SP_TA_1 : (MSPTA1 : M_SP_TA_1_R);
      M_DP_NA_1 : (MDPNA1 : M_DP_NA_1_R);
      M_DP_TA_1 : (MDPTA1 : M_DP_TA_1_R);
      M_ST_NA_1 : (MSTNA1 : M_ST_NA_1_R);
      M_ST_TA_1 : (MSTTA1 : M_ST_TA_1_R);
      M_BO_NA_1 : (MBONA1 : M_BO_NA_1_R);
      M_BO_TA_1 : (MBOTA1 : M_BO_TA_1_R);
      M_ME_NA_1 : (MMENA1 : M_ME_NA_1_R);
      M_ME_TA_1 : (MMETA1 : M_ME_TA_1_R);
      M_ME_NB_1 : (MMENB1 : M_ME_NB_1_R);
      M_ME_TB_1 : (MMETB1 : M_ME_TB_1_R);
      M_ME_NC_1 : (MMENC1 : M_ME_NC_1_R);
      M_ME_TC_1 : (MMETC1 : M_ME_TC_1_R);
      M_IT_NA_1 : (MITNA1 : M_IT_NA_1_R);
      M_IT_TA_1 : (MITTA1 : M_IT_TA_1_R);
      M_EP_TA_1 : (MEPTA1 : M_EP_TA_1_R);
      M_EP_TB_1 : (MEPTB1 : M_EP_TB_1_R);
      M_EP_TC_1 : (MEPTC1 : M_EP_TC_1_R);
      M_PS_NA_1 : (MPSNA1 : M_PS_NA_1_R);
      M_ME_ND_1 : (MMEND1 : M_ME_ND_1_R);
      M_SP_TB_1 : (MSPTB1 : M_SP_TB_1_R);
      M_DP_TB_1 : (MDPTB1 : M_DP_TB_1_R);
      M_ST_TB_1 : (MSTTB1 : M_ST_TB_1_R);
      M_BO_TB_1 : (MBOTB1 : M_BO_TB_1_R);
      M_ME_TD_1 : (MMETD1 : M_ME_TD_1_R);
      M_ME_TE_1 : (MMETE1 : M_ME_TE_1_R);
      M_ME_TF_1 : (MMETF1 : M_ME_TF_1_R);
      M_IT_TB_1 : (MITTB1 : M_IT_TB_1_R);
      M_EP_TD_1 : (MEPTD1 : M_EP_TD_1_R);
      M_EP_TE_1 : (MEPTE1 : M_EP_TE_1_R);
      M_EP_TF_1 : (MEPTF1 : M_EP_TF_1_R);
      C_SC_NA_1 : (CSCNA1 : C_SC_NA_1_R);
      C_DC_NA_1 : (CDCNA1 : C_DC_NA_1_R);
      C_RC_NA_1 : (CRCNA1 : C_RC_NA_1_R);
      C_SE_NA_1 : (CSENA1 : C_SE_NA_1_R);
      C_SE_NB_1 : (CSENB1 : C_SE_NB_1_R);
      C_SE_NC_1 : (CSENC1 : C_SE_NC_1_R);
      C_BO_NA_1 : (CBONA1 : C_BO_NA_1_R);
      //C_SC_TA_1 : (CSCTA1 : C_SC_TA_1_R);
      //C_DC_TA_1 : (CDCTA1 : C_DC_TA_1_R);
      //C_RC_TA_1 : (CRCTA1 : C_RC_TA_1_R);
      //C_SE_TA_1 : (CSETA1 : C_SE_TA_1_R);
      //C_SE_TB_1 : (CSETB1 : C_SE_TB_1_R);
      //C_SE_TC_1 : (CSETC1 : C_SE_TC_1_R);
      //C_BO_TA_1 : (CBOTA1 : C_BO_TA_1_R);
      M_EI_NA_1 : (MEINA1 : M_EI_NA_1_R);
      C_IC_NA_1 : (CICNA1 : C_IC_NA_1_R);
      C_CI_NA_1 : (CCINA1 : C_CI_NA_1_R);
      C_RD_NA_1 : (CRDNA1 : C_RD_NA_1_R);
      C_CS_NA_1 : (CCSNA1 : C_CS_NA_1_R);
      C_TS_NA_1 : (CTSNA1 : C_TS_NA_1_R);
      C_RP_NA_1 : (CRPNA1 : C_RP_NA_1_R);
      C_CD_NA_1 : (CCDNA1 : C_CD_NA_1_R);
      //C_TS_TA_1 : (CTSTA1 : C_TS_TA_1_R);
      P_ME_NA_1 : (PMENA1 : P_ME_NA_1_R);
      P_ME_NB_1 : (PMENB1 : P_ME_NB_1_R);
      P_ME_NC_1 : (PMENC1 : P_ME_NC_1_R);
      P_AC_NA_1 : (PACNA1 : P_AC_NA_1_R);
      F_FR_NA_1 : (FFRNA1 : F_FR_NA_1_R);
      F_SR_NA_1 : (FSRNA1 : F_SR_NA_1_R);
      F_SC_NA_1 : (FSCNA1 : F_SC_NA_1_R);
      F_LS_NA_1 : (FLSNA1 : F_LS_NA_1_R);
      F_AF_NA_1 : (FAFNA1 : F_AF_NA_1_R);
      F_SG_NA_1 : (FSGNA1 : F_SG_NA_1_R);
      F_DR_TA_1 : (FDRTA1 : F_DR_TA_1_R);
      //F_SC_NB_1 : (FSCNB1 : F_SC_NB_1_R);
    end;

    ApduDescription_R = record
      Iec104              : Iec104_R;
      AsduType            : AsduTypes_E;
      StrcQual            : StructureQualifier_R;
      CauseTrans          : CauseOfTransmittion_R;
      OriginatorAdderess  : Byte;
      ASDUAddress         : Integer;
      InfObjAddress       : Integer; // used if  StrcQual.SQ = 1
      InfObjDescr         : array [0..VSQ_MAX] of InfObjDescr_R;
    end;

    procedure Init_ApduDescription(var desct : ApduDescription_R);

implementation

  procedure Init_ApduDescription(var desct : ApduDescription_R);
  var
    fields : array[0..sizeof(ApduDescription_R) - 1] of byte absolute desct;
    i      : Integer;
  begin
    for i := 0 to sizeof(ApduDescription_R) - 1 do  fields[i] := 0;
  end;


end.

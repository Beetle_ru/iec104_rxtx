unit InformObjFields;

interface

uses Tools;

type
    SIQ_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property SPI : Byte index $0001 read GetBits write SetBits; // [0]    b1
      property RES : Byte index $0103 read GetBits write SetBits; // [1..3] b1
      property BL  : Byte index $0401 read GetBits write SetBits; // [4]    b1
      property SB  : Byte index $0501 read GetBits write SetBits; // [5]    b1
      property NT  : Byte index $0601 read GetBits write SetBits; // [6]    b1
      property IV  : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    DIQ_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property DPI : Byte index $0002 read GetBits write SetBits; // [0..1] b1
      property RES : Byte index $0202 read GetBits write SetBits; // [2..3] b1
      property BL  : Byte index $0401 read GetBits write SetBits; // [4]    b1
      property SB  : Byte index $0501 read GetBits write SetBits; // [5]    b1
      property NT  : Byte index $0601 read GetBits write SetBits; // [6]    b1
      property IV  : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    QDS_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property OV  : Byte index $0001 read GetBits write SetBits; // [0]    b1
      property RES : Byte index $0103 read GetBits write SetBits; // [1..3] b1
      property BL  : Byte index $0401 read GetBits write SetBits; // [4]    b1
      property SB  : Byte index $0501 read GetBits write SetBits; // [5]    b1
      property NT  : Byte index $0601 read GetBits write SetBits; // [6]    b1
      property IV  : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    QDP_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property RES : Byte index $0003 read GetBits write SetBits; // [0..2] b1
      property EI  : Byte index $0301 read GetBits write SetBits; // [3]    b1
      property BL  : Byte index $0401 read GetBits write SetBits; // [4]    b1
      property SB  : Byte index $0501 read GetBits write SetBits; // [5]    b1
      property NT  : Byte index $0601 read GetBits write SetBits; // [6]    b1
      property IV  : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    VTI_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Value           : Byte index $0007 read GetBits write SetBits; // [0..6] b1
      property TransitionState : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    NVA_R = record
    public
      BitField : array[0..1] of Byte; // 2 bytes
    private
      function  GetBits(const aIndex: Integer): single;
      procedure SetBits(const aIndex: Integer; const aValue: single);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Value : single index $0010 read GetBits write SetBits; // [0..15] b1, b2
    end;

//******************************************************************************

    SVA_R = record
    public
      BitField : array[0..1] of Byte; // 2 bytes
    private
      function  GetBits(const aIndex: Integer): Integer;
      procedure SetBits(const aIndex: Integer; const aValue: Integer);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Value : Integer index $0010 read GetBits write SetBits; // [0..15] b1, b2
    end;

//******************************************************************************

    R32IEEE754_R = record
    public
      BitField : array[0..3] of Byte; // 4 bytes
    private
      function  GetBits(const aIndex: Integer): single;
      procedure SetBits(const aIndex: Integer; const aValue: single);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Value : single index $0020 read GetBits write SetBits; // [0..31] b1, b2, b3, b4
    end;

//******************************************************************************

    BCR_R = record
    public
      BitField : array[0..4] of Byte; // 5 bytes
    private
      function  GetBits(const aIndex: Integer): Integer;
      procedure SetBits(const aIndex: Integer; const aValue: Integer);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Value : Integer index $0020 read GetBits write SetBits; // [0..31]  b1, b2, b3, b4
      property SQ    : Integer index $2005 read GetBits write SetBits; // [32..36] b5
      property CI    : Integer index $2501 read GetBits write SetBits; // [37]     b5
      property CA    : Integer index $2601 read GetBits write SetBits; // [38]     b5
      property IV    : Integer index $2701 read GetBits write SetBits; // [39]     b5
    end;

//******************************************************************************

    SEP_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property ES : Byte index $0002 read GetBits write SetBits; // [0..1] b1
      property RES: Byte index $0201 read GetBits write SetBits; // [2]    b1
      property EI : Byte index $0301 read GetBits write SetBits; // [3]    b1
      property BL : Byte index $0401 read GetBits write SetBits; // [4]    b1
      property SB : Byte index $0501 read GetBits write SetBits; // [5]    b1
      property NT : Byte index $0601 read GetBits write SetBits; // [6]    b1
      property IV : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    SPE_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property GS  : Byte index $0001 read GetBits write SetBits; // [0] b1
      property SL1 : Byte index $0101 read GetBits write SetBits; // [1] b1
      property SL2 : Byte index $0201 read GetBits write SetBits; // [2] b1
      property SL3 : Byte index $0301 read GetBits write SetBits; // [3] b1
      property SIE : Byte index $0401 read GetBits write SetBits; // [4] b1
      property SRD : Byte index $0501 read GetBits write SetBits; // [5] b1
      property RES : Byte index $0601 read GetBits write SetBits; // [6] b1
    end;

//******************************************************************************

    OCI_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property GC  : Byte index $0001 read GetBits write SetBits; // [0]    b1
      property CL1 : Byte index $0101 read GetBits write SetBits; // [1]    b1
      property CL2 : Byte index $0201 read GetBits write SetBits; // [2]    b1
      property CL3 : Byte index $0301 read GetBits write SetBits; // [3]    b1
      property RES : Byte index $0404 read GetBits write SetBits; // [4..7] b1
    end;

//******************************************************************************

    BSI_R = record
    public
      BitField : array[0..3] of Byte; // 4 bytes
    private
      function  GetBits(const aIndex: Integer): LongWord;
      procedure SetBits(const aIndex: Integer; const aValue: LongWord);
    public
      // index oobb. oo -- offset, bb -- use bits
      property B32 : LongWord index $0020 read GetBits write SetBits; // [0..31] b1, b2, b3, b4
    end;

//******************************************************************************

    FBP_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Word;
      procedure SetBits(const aIndex: Integer; const aValue: Word);
    public
      // index oobb. oo -- offset, bb -- use bits
      property UI16 : Word index $0010 read GetBits write SetBits; // [0..15] b1, b2
    end;

//*********************** COMMANDS *********************************************

    SCO_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property UI16 : Byte  index $0001 read GetBits write SetBits;   // [0]    b1
      property RES  : Byte  index $0101 read GetBits write SetBits;   // [1]    b1
      // QOC
      property QOC_QU  : Byte index $0205 read GetBits write SetBits; // [2..6] b1
      property QOC_SE  : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    DCO_R = record
    public
      BitField : array[0..0] of Byte; // 1 bytes
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property DCS : Byte  index $0002 read GetBits write SetBits;    // [0..1] b1
      // QOC
      property QOC_QU  : Byte index $0205 read GetBits write SetBits; // [2..6] b1
      property QOC_SE  : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    RCO_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property RCS : Byte  index $0002 read GetBits write SetBits;    // [0..1] b1
      // QOC
      property QOC_QU  : Byte index $0205 read GetBits write SetBits; // [2..6] b1
      property QOC_SE  : Byte index $0701 read GetBits write SetBits; // [7]    b1
    end;

//******************************************************************************

    Time2_R = record
    public
      BitField : array[0..6] of Byte; // 7 bytes
    private
      function  GetBits(const aIndex: Integer): Word;
      procedure SetBits(const aIndex: Integer; const aValue: Word);
    public
      // index oobb. oo -- offset, bb -- use bits
      property MilliSeconds : Word  index $0010 read GetBits write SetBits; // [0..15]  b1, b2
    end;

//******************************************************************************

    Time3_R = record
    public
      BitField : array[0..6] of Byte; // 7 bytes
    private
      function  GetBits(const aIndex: Integer): Word;
      procedure SetBits(const aIndex: Integer; const aValue: Word);
    public
      // index oobb. oo -- offset, bb -- use bits
      property MilliSeconds : Word  index $0010 read GetBits write SetBits; // [0..15]  b1, b2
      property Minutes      : Word  index $1006 read GetBits write SetBits; // [16..21] b3
      property RES1         : Word  index $1601 read GetBits write SetBits; // [22]     b3
      property IV           : Word  index $1701 read GetBits write SetBits; // [23]     b3
    end;

//******************************************************************************

    Time7_R = record
    public
      BitField : array[0..6] of Byte; // 7 bytes
    private
      function  GetBits(const aIndex: Integer): Word;
      procedure SetBits(const aIndex: Integer; const aValue: Word);
    public
      // index oobb. oo -- offset, bb -- use bits
      property MilliSeconds : Word  index $0010 read GetBits write SetBits; // [0..15]  b1, b2
      property Minutes      : Word  index $1006 read GetBits write SetBits; // [16..21] b3
      property RES1         : Word  index $1601 read GetBits write SetBits; // [22]     b3
      property IV           : Word  index $1701 read GetBits write SetBits; // [23]     b3
      property Hours        : Word  index $1805 read GetBits write SetBits; // [24..28] b4
      property RES2         : Word  index $1d02 read GetBits write SetBits; // [29..30] b4
      property SU           : Word  index $1f10 read GetBits write SetBits; // [31]     b4
      property DayOfMonth   : Word  index $2005 read GetBits write SetBits; // [32..36] b5
      property DayOfWeek    : Word  index $2503 read GetBits write SetBits; // [37..39] b5
      property Months       : Word  index $2805 read GetBits write SetBits; // [40..44] b6
      property RES3         : Word  index $2d03 read GetBits write SetBits; // [45..47] b6
      property Years        : Word  index $3007 read GetBits write SetBits; // [48..54] b7
      property RES4         : Word  index $3701 read GetBits write SetBits; // [55]     b7
    end;

//******************************************************************************

    COI_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Cause : Byte  index $0007 read GetBits write SetBits;    // [0..6] b1
      property RES   : Byte  index $0701 read GetBits write SetBits;    // [0..1] b1
    end;

//******************************************************************************

    QOI_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Pointer : Byte  index $0008 read GetBits write SetBits;  // [0..6] b1
    end;

//******************************************************************************

    QCC_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property RQT : Byte  index $0006 read GetBits write SetBits;  // [0..5] b1
      property FRZ : Byte  index $0602 read GetBits write SetBits;  // [6..7] b1
    end;

//******************************************************************************

    QPM_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property KPA : Byte  index $0006 read GetBits write SetBits;  // [0..5] b1
      property LPC : Byte  index $0601 read GetBits write SetBits;  // [6]    b1
      property POP : Byte  index $0701 read GetBits write SetBits;  // [7]    b1
    end;

//******************************************************************************

    QPA_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Pointer : Byte  index $0008 read GetBits write SetBits;  // [0..7] b1
    end;

//******************************************************************************

    QRP_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Pointer : Byte  index $0008 read GetBits write SetBits;  // [0..7] b1
    end;

//******************************************************************************

    FRQ_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Pointer : Byte  index $0007 read GetBits write SetBits;  // [0..6] b1
      property Confirm : Byte  index $0701 read GetBits write SetBits;  // [7]    b1
    end;

//******************************************************************************

    SRQ_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Pointer : Byte  index $0007 read GetBits write SetBits;  // [0..6] b1
      property Status  : Byte  index $0701 read GetBits write SetBits;  // [7]    b1
    end;

//******************************************************************************

    SCQ_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property SelectorP : Byte  index $0004 read GetBits write SetBits;  // [0..3] b1
      property CallP     : Byte  index $0404 read GetBits write SetBits;  // [4..7] b1
    end;

//******************************************************************************

    LSQ_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Pointer : Byte  index $0008 read GetBits write SetBits;  // [0..7] b1
    end;

//******************************************************************************

    AFQ_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property FileP    : Byte  index $0004 read GetBits write SetBits;  // [0..3] b1
      property SectionP : Byte  index $0004 read GetBits write SetBits;  // [4..7] b1
    end;

//******************************************************************************

    NOF_R = record
    public
      BitField : array[0..1] of Byte; // 2 bytes
    private
      function  GetBits(const aIndex: Integer): Word;
      procedure SetBits(const aIndex: Integer; const aValue: Word);
    public
      // index oobb. oo -- offset, bb -- use bits
      property FileName  : Word  index $0010 read GetBits write SetBits;  // [0..15] b1
    end;

//******************************************************************************

    NOS_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property SectionName  : Byte  index $0008 read GetBits write SetBits;  // [0..7] b1
    end;

//******************************************************************************

    LOF_R = record
    public
      BitField : array[0..2] of Byte; // 3 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Length  : Byte  index $0018 read GetBits write SetBits;  // [0..23] b1
    end;

//******************************************************************************

    LOS_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property SementLen  : Byte  index $0008 read GetBits write SetBits;  // [0..7] b1
    end;

//******************************************************************************

    CHS_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property ControllSumm  : Byte  index $0008 read GetBits write SetBits;  // [0..7] b1
    end;

//******************************************************************************

    SOF_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property Status : Byte  index $0004 read GetBits write SetBits;  // [0..3] b1
      property LFD    : Byte  index $0501 read GetBits write SetBits;  // [5]    b1
      property FOR_   : Byte  index $0601 read GetBits write SetBits;  // [6]    b1
      property FA     : Byte  index $0701 read GetBits write SetBits;  // [7]    b1
    end;

//******************************************************************************

    QOS_R = record
    public
      BitField : array[0..0] of Byte; // 1 byte
    private
      function  GetBits(const aIndex: Integer): Byte;
      procedure SetBits(const aIndex: Integer; const aValue: Byte);
    public
      // index oobb. oo -- offset, bb -- use bits
      property QL : Byte  index $0006 read GetBits write SetBits;  // [0..6] b1
      property SE : Byte  index $0701 read GetBits write SetBits;  // [7]    b1
    end;

//******************************************************************************

    SCD_R = record
    public
      BitField : array[0..3] of Byte; // 4 byte
    private
      function  GetBits(const aIndex: Integer): Word;
      procedure SetBits(const aIndex: Integer; const aValue: Word);
    public
      // index oobb. oo -- offset, bb -- use bits
      property ST : Word  index $0010 read GetBits write SetBits;  // [0..15]   b1, b2
      property CD : Word  index $1010 read GetBits write SetBits;  // [15..31]  b3, b4
    end;

implementation

//******************************************************************************
//******************** SetBits and GetBits realization *************************
//******************************************************************************

//******************** SIQ_R ***************************************************
  function SIQ_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SIQ_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** DIQ_R ***************************************************
  function DIQ_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure DIQ_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QDS_R ***************************************************
  function QDS_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QDS_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QDP_R ***************************************************
  function QDP_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QDP_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** BCR_R ***************************************************
  function BCR_R.GetBits(const aIndex: Integer): Integer;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure BCR_R.SetBits(const aIndex: Integer; const aValue: Integer);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** VTI_R ***************************************************
  function VTI_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure VTI_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** NVA_R ***************************************************
  function NVA_R.GetBits(const aIndex: Integer): single;
  var
    lookLikeInt : SmallInt;
    k : integer;
  begin
    lookLikeInt := GetBitField(BitField, aIndex);
    if QueryBit(BitField[1], SelectBit(7)) then k := 32768
    else k := 32767;

    Result := lookLikeInt / k;
  end;

  procedure NVA_R.SetBits(const aIndex: Integer; const aValue: single);
  var
    lookLikeInt : SmallInt;
    k : integer;
  begin
    if aValue < 0 then k := 32768
    else k := 32767;
    lookLikeInt := round(aValue * k);
    SetBitField(BitField, aIndex, lookLikeInt);
  end;

//******************** SVA_R ***************************************************
  function SVA_R.GetBits(const aIndex: Integer): Integer;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SVA_R.SetBits(const aIndex: Integer; const aValue: Integer);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** R32IEEE754_R ********************************************
  function R32IEEE754_R.GetBits(const aIndex: Integer): single;
  var
    lookLikeInt : Cardinal absolute Result;
  begin
    lookLikeInt := GetBitField(BitField, aIndex);
  end;

  procedure R32IEEE754_R.SetBits(const aIndex: Integer; const aValue: single);
  var
    lookLikeInt : Cardinal absolute aValue;
  begin
    SetBitField(BitField, aIndex, lookLikeInt);
  end;

//******************** SEP_R ***************************************************
  function SEP_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SEP_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** SPE_R ***************************************************
  function SPE_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SPE_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** OCI_R ***************************************************
  function OCI_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure OCI_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** BSI_R ***************************************************
  function BSI_R.GetBits(const aIndex: Integer): LongWord;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure BSI_R.SetBits(const aIndex: Integer; const aValue: LongWord);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** FBP_R ***************************************************
  function FBP_R.GetBits(const aIndex: Integer): Word;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure FBP_R.SetBits(const aIndex: Integer; const aValue: Word);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** SCO_R ***************************************************
  function SCO_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SCO_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** DCO_R ***************************************************
  function DCO_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure DCO_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** RCO_R ***************************************************
  function RCO_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure RCO_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** Time2_R *************************************************
  function Time2_R.GetBits(const aIndex: Integer): Word;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure Time2_R.SetBits(const aIndex: Integer; const aValue: Word);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** Time3_R *************************************************
  function Time3_R.GetBits(const aIndex: Integer): Word;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure Time3_R.SetBits(const aIndex: Integer; const aValue: Word);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** Time7_R *************************************************
  function Time7_R.GetBits(const aIndex: Integer): Word;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure Time7_R.SetBits(const aIndex: Integer; const aValue: Word);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** COI_R ***************************************************
  function COI_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure COI_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QOI_R ***************************************************
  function QOI_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QOI_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QCC_R ***************************************************
  function QCC_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QCC_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QPM_R ***************************************************
  function QPM_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QPM_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QPA_R ***************************************************
  function QPA_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QPA_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QRP_R ***************************************************
  function QRP_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QRP_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** FRQ_R ***************************************************
  function FRQ_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure FRQ_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** SRQ_R ***************************************************
  function SRQ_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SRQ_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** SCQ_R ***************************************************
  function SCQ_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SCQ_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** LSQ_R ***************************************************
  function LSQ_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure LSQ_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** AFQ_R ***************************************************
  function AFQ_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure AFQ_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** NOF_R ***************************************************
  function NOF_R.GetBits(const aIndex: Integer): Word;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure NOF_R.SetBits(const aIndex: Integer; const aValue: Word);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** NOS_R ***************************************************
  function NOS_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure NOS_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** LOF_R ***************************************************
  function LOF_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure LOF_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** LOS_R ***************************************************
  function LOS_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure LOS_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** CHS_R ***************************************************
  function CHS_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure CHS_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** SOF_R ***************************************************
  function SOF_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SOF_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** QOS_R ***************************************************
  function QOS_R.GetBits(const aIndex: Integer): Byte;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure QOS_R.SetBits(const aIndex: Integer; const aValue: Byte);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

//******************** SCD_R ***************************************************
  function SCD_R.GetBits(const aIndex: Integer): Word;
  begin
    Result := GetBitField(BitField, aIndex);
  end;

  procedure SCD_R.SetBits(const aIndex: Integer; const aValue: Word);
  begin
    SetBitField(BitField, aIndex, aValue);
  end;

end.

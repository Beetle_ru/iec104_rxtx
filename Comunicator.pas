unit Comunicator;

interface

uses
  IecSerializer,
  ASDUData,
  classes,
  sysUtils,
  ASDU,
  Tools,
  syncobjs,
  InformObj,
  InformObjFields,
  InformObjConst,
  InformObjStructures,
  SocConFPC;

  procedure PrintAPDU(apdu_Dat : ApduBuffer_T);


type
  Direction_T = (MASTER, SLAVE);

  TransmitionData_R = record
    R,S : Integer;
  end;

  Timer_C = class(TThread)
  public
    HandlerTimer     : procedure(value : TObject) of object;
    HandlerTimerProc : procedure(value : TObject);
    Interval         : Integer;
    value            : TObject;

  private
    IsExecuting      : Boolean;
    IsReseted        : Boolean;

  public
    procedure Execute(); override;
    procedure StopTimer();
    procedure StartTimer();
  end;

  ActualValues_C = class(TObject)
    Id : Integer;
  end;

  IEC104_C = class(TObject)
  public
    UserValueHandler    : procedure(descr : ApduDescription_R; id : Integer) of object;
    UserValueHandlerPoc : procedure(descr : ApduDescription_R; id : Integer);
    OnConnectedHandler  : procedure(id : Integer) of object;
    OriginalAddressOctets          : 0..1; // manual setting
    AsudAddresOctets               : 1..2; // manual setting
    ObjectInformationAddressOctets : 1..3; // manual setting
    Iec104Ser_obj                  : Iec104Ser_C; // TODO make private

  private
    Direction_m : Direction_T;
    Master      : IecMaster_C;
    Slave       : IecSlave_C;
    SSenders    : Array[0..MaxConnections] of Timer_C;
    RS_Verifyer : Timer_C;
    TransDat    : array[0..MaxConnections] of TransmitionData_R;
    CS          : TCriticalSection;
    RS_CS       : TCriticalSection;

  public
    constructor Create(Direction : Direction_T; ip, port : string);
    destructor  Destroy();
    procedure   PushSingle(descr : ApduDescription_R; id : Integer);

  private
    procedure MasterReceiveHandler(buf : ApduBuffer_T; id : Integer);
    procedure SlaveReceiveHandler(buf : ApduBuffer_T; id : Integer);
    procedure SplitParse(var buf : ApduBuffer_T; id : Integer);
    procedure Uhandler(operation : UCommands_E; id : Integer);
    procedure Ihandler(descr : ApduDescription_R; id : Integer);
    procedure Shandler(sequenceNumberR : Integer; id : Integer);
    procedure Conected(id : Integer);
    procedure SendS(value : TObject);
    procedure RS_Verify(value : TObject);
    procedure SARTDT();
  end;

implementation
  constructor IEC104_C.Create(Direction : Direction_T; ip, port : string);
  var
    i  : Integer;
    av : ActualValues_C;
  begin
    Direction_m              := Direction;

    Iec104Ser_obj            := Iec104Ser_C.Create();
    Iec104Ser_obj.U_Handler  := self.Uhandler;
    Iec104Ser_obj.I_Handler  := self.Ihandler;
    Iec104Ser_obj.S_Handler  := self.Shandler;

    CS                       := TCriticalSection.Create;
    RS_CS                    := TCriticalSection.Create;

    for i := 0 to MaxConnections do
    begin
      SSenders[i]              := Timer_C.Create(true);
      SSenders[i].HandlerTimer := SendS;
      SSenders[i].Interval     := 1000; // TODO make setting
      av                       :=  ActualValues_C.Create;
      av.Id                    := i;
      SSenders[i].value        := av;
    end;

    RS_Verifyer              := Timer_C.Create(true);
    RS_Verifyer.HandlerTimer := RS_Verify;
    RS_Verifyer.Interval     := 15000; // TODO make setting
    RS_Verifyer.value        := nil;

    if Direction = Direction_T.MASTER then
    begin
      Master             := IecMaster_C.Create(false);
      Master.address     := ip;
      Master.port        := port;
      Master.OnReceive   := self.MasterReceiveHandler;
      Master.OnConnected := Conected;
    end;

    if Direction = Direction_T.SLAVE then
    begin
      Slave              := IecSlave_C.Create(false);
      Slave.port         := port;
      Slave.OnReceive    := SlaveReceiveHandler;
      Slave.OnConnected  := Conected;
    end;

  end;

  destructor IEC104_C.Destroy();
  var
    i : Integer;
  begin
    inherited;
    Master.Free;  // TODO
    Slave.Terminate;
    Iec104Ser_obj.Free;
    RS_Verifyer.Terminate;

    for i := 0 to MaxConnections do
    begin
      SSenders[i].Terminate;
    end;
  end;

  procedure IEC104_C.MasterReceiveHandler(buf : ApduBuffer_T; id : Integer);
  begin
    //PrintAPDU(buf);
    CS.Enter;
    SplitParse(buf, id);
    CS.Leave;
  end;

  procedure IEC104_C.SlaveReceiveHandler(buf : ApduBuffer_T; id : Integer);
  begin
    //PrintAPDU(buf);
    CS.Enter;
    SplitParse(buf, id);
    CS.Leave;
  end;

  procedure IEC104_C.SplitParse(var buf : ApduBuffer_T; id : Integer);
  const
    APCI_HeaderLen  = 2;
    APCI_StartShift = 0;
    APCI_LenShift   = 1;
  var
     bufferLen     : Integer;
     iecPkgBegin   : Integer;
     iecPkgFullLen : Integer;
     APDU_Buf      : ApduBuffer_T;
  begin
    if not(buf[APCI_StartShift] = $68) then Exit; // not iec104 packet

    bufferLen     := Length(buf);
    iecPkgBegin   := APCI_StartShift;
    iecPkgFullLen := 0;

    while iecPkgFullLen + iecPkgBegin < bufferLen do
    begin
      iecPkgFullLen := buf[APCI_LenShift + iecPkgBegin] + APCI_HeaderLen;

      if not(buf[iecPkgBegin] = $68) then Exit;                // not iec104 packet
      if (iecPkgFullLen + iecPkgBegin) >= bufferLen then Exit; // buffer have not the inough data

      CPArray(APDU_Buf, buf, iecPkgFullLen, 0, iecPkgBegin);

      Iec104Ser_obj.Parse(APDU_Buf, id);
      iecPkgBegin := iecPkgBegin + iecPkgFullLen;
    end;
  end;

  procedure IEC104_C.Conected(id : Integer);
  begin
    RS_CS.Enter;
    TransDat[id].R := 0;
    TransDat[id].S := 0;
    //writeln('^^Conected id = ', id, ' ^^');
    RS_CS.Leave;
    if @OnConnectedHandler <> nil then OnConnectedHandler(id);

  end;

  procedure IEC104_C.Uhandler(operation : UCommands_E; id : Integer);
  begin
    //writeln('U_HANDLER:');
    case operation  of
      UCommands_E.U_STARTDT_ACT :
      begin
        //writeln(' operation = STARTDT_ACT, id = ', id);
        Iec104Ser_obj.GenerateU(UCommands_E.U_STARTDT_CON);
        if self.Direction_m = Direction_T.SLAVE then
        begin
          Slave.Send(Iec104Ser_obj.APDU_Dat, Iec104Ser_obj.APDU_Len, id);
        end;
      end;

      UCommands_E.U_STARTDT_CON :
      begin
        //writeln(' operation = U_STARTDT_CON');
      end;

      UCommands_E.U_STOPDT_ACT :
      begin
        //writeln(' operation = U_STOPDT_ACT');
      end;

      UCommands_E.U_STOPDT_CON :
      begin
        //writeln(' operation = U_STOPDT_CON');
      end;

      UCommands_E.U_TESTFR_ACT :
      begin
        //writeln(' operation = U_TESTFR_ACT id = ', id);
        Iec104Ser_obj.GenerateU(UCommands_E.U_TESTFR_CON);
        if self.Direction_m = Direction_T.SLAVE then
        begin
          Slave.Send(Iec104Ser_obj.APDU_Dat, Iec104Ser_obj.APDU_Len, 0);
        end;
      end;

      UCommands_E.U_TESTFR_CON :
      begin
        //writeln(' operation = U_TESTFR_CON');
      end;
    end;
  end;

  procedure IEC104_C.Ihandler(descr : ApduDescription_R; id : Integer);
  begin
    //writeln('I_HANDLER: type = ', Integer(descr.AsduType));

    SSenders[id].StopTimer;
    SSenders[id].StartTimer;

    RS_CS.Enter;
    //write('<<< LOCAL(S:',TransDat[id].S, '; R:', TransDat[id].R, ')');
    //writeln('<<< TXRX(S:', descr.Iec104.SequenceNumberS, '; R:', descr.Iec104.SequenceNumberR, ')');
    Inc(TransDat[id].R);
    RS_CS.Leave;

    // call user value handler
    if @UserValueHandler    <> nil then UserValueHandler(descr, id);
    if @UserValueHandlerPoc <> nil then UserValueHandlerPoc(descr, id);
  end;

  procedure IEC104_C.Shandler(sequenceNumberR : Integer; id : Integer);
  begin
    //writeln('S_HANDLER:');
    RS_CS.Enter;

    RS_CS.Leave;
  end;


  procedure IEC104_C.PushSingle(descr : ApduDescription_R; id : Integer);
  begin
    RS_CS.Enter;
    descr.Iec104.SequenceNumberS := TransDat[id].S;
    descr.Iec104.SequenceNumberR := TransDat[id].R;

    //write  ('>>> LOCAL(S:',TransDat[id].S, '; R:', TransDat[id].R, ')');
    //writeln('>>> TXRX(S:', descr.Iec104.SequenceNumberS, '; R:', descr.Iec104.SequenceNumberR, ')');

    Inc(TransDat[id].S);

    RS_CS.Leave;

    Iec104Ser_obj.GenerateI(descr);

    RS_Verifyer.StopTimer;
    RS_Verifyer.StartTimer;

    if  Direction_m = Direction_T.MASTER then
    begin
      Master.Send(Iec104Ser_obj.APDU_Dat, Iec104Ser_obj.APDU_Len);
    end else if  Direction_m = Direction_T.SLAVE then
      Slave.Send(Iec104Ser_obj.APDU_Dat, Iec104Ser_obj.APDU_Len, id);
    begin
    end;
  end;

  procedure IEC104_C.SARTDT();
  begin
    Iec104Ser_obj.GenerateU(UCommands_E.U_STARTDT_ACT);
    if self.Direction_m = Direction_T.MASTER then
    begin
      Master.Send(Iec104Ser_obj.APDU_Dat, Iec104Ser_obj.APDU_Len - 1);
    end;
  end;

  procedure IEC104_C.SendS(value : TObject);
  var
    id : Integer;
  begin
    id := (value as ActualValues_C).Id;
    RS_CS.Enter;
    Iec104Ser_obj.GenerateS(TransDat[id].R);
    RS_CS.Leave;

    if self.Direction_m = Direction_T.SLAVE then
    begin
      slave.Send(Iec104Ser_obj.APDU_Dat, Iec104Ser_obj.APDU_Len, id);
    end else if self.Direction_m = Direction_T.MASTER then
    begin
      master.Send(Iec104Ser_obj.APDU_Dat, Iec104Ser_obj.APDU_Len);
    end;

    ////writeln('SSend ^^^^^^ id = ', id);

    SSenders[id].StopTimer;
  end;

  procedure IEC104_C.RS_Verify(value : TObject);
  begin
    RS_Verifyer.StopTimer;

    RS_CS.Enter;

    RS_CS.Leave;
  end;

//******************************************************************************
//******************************************************************************
//******************************************************************************

  procedure Timer_C.Execute();
  begin
    self.FreeOnTerminate := True;
    while not self.Terminated do
    begin
      if IsExecuting then IsReseted := True;

      sleep(Interval);

      if IsExecuting and IsReseted then
      begin
        if @HandlerTimer <> nil then HandlerTimer(value);
        if @HandlerTimerProc <> nil then HandlerTimerProc(value);
      end;

    end;
  end;

  procedure Timer_C.StopTimer();
  begin
    IsExecuting := False;
    IsReseted   := False;
  end;

  procedure Timer_C.StartTimer();
  begin
    Suspended   := False;
    IsExecuting := True;
  end;

//******************************************************************************
//******************************************************************************
//******************************************************************************

  procedure PrintAPDU(apdu_Dat : ApduBuffer_T);
  const
    TERMINAL_STR_LEN = 20;
  var
    i  : Integer;
    ln : Integer;
  begin
    i := 0;

    while i < APDU_MAX_LEN do
    begin
      for ln := i to i + TERMINAL_STR_LEN do
      begin

        if ln > APDU_MAX_LEN then  break;

      end;
      i := i + TERMINAL_STR_LEN;
    end;
  end;
end.

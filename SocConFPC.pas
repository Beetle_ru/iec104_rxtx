unit SocConFPC;

interface
uses   classes, Sockets, sysUtils, SyncObjs, IecSerializer,
   blcksock, Synautil;

const
  QueueSize      = 50;
  MaxConnections = 10;

type
  Receive_D   = procedure(buf : ApduBuffer_T; id : Integer) of object;
  Connected_D = procedure(id : Integer) of object;

//******************************************************************************

  IecMaster_C = class(TThread)
  public
    OnReceive   : Receive_D;
    OnConnected : Connected_D;
    address     : string;
    port        : string;

  private
    tcp         : TTCPBlockSocket;

  public
    procedure Send(buf : ApduBuffer_T; len : Integer);

  protected
    procedure Execute(); override;
  end;

//******************************************************************************

  QueueItem_R = record
    Buf         : ApduBuffer_T;
    Id          : Integer;
    IsUnhandled : Boolean;
  end;

//******************************************************************************

  Executor_C = class(TThread)
  public
    OnExec : Receive_D;

  private
    Cnt    : Integer;
    Queue  : array[0..QueueSize] of QueueItem_R;
    CS     : TCriticalSection;

  public
    procedure Enqueue(buf : ApduBuffer_T; id : Integer);

  protected
    procedure Execute(); override;
  end;

//******************************************************************************

  ConnectProxy_C = class(TThread)
  private
    OnExec           : procedure(buf : ApduBuffer_T; id : Integer) of object;
    TcpConn          : TTCPBlockSocket;
    IsConnectionOpnd : Boolean;
    Queue            : Executor_C;
    Id               : Integer;

  public
    constructor Create(Hsock : Integer; connectId : Integer; var execQueue : Executor_C);
    procedure Send(buf : ApduBuffer_T; Len : Integer);

  protected
    procedure Execute(); override;
  end;

//******************************************************************************

  IecSlave_C = class(TThread)
  public
    address     : string;
    port        : string;
    OnReceive   : Receive_D;
    OnConnected : Connected_D;
    OnSended    : procedure() of object;

  private
    tcp         : TTCPBlockSocket;
    Pa          : array[0..MaxConnections] of ConnectProxy_C;
    ServerSock  : TTCPBlockSocket;
    Queue       : Executor_C;

  public
    procedure Send(buf : ApduBuffer_T; Len, id : Integer);

  protected
    procedure Execute(); override;
  end;

implementation
//******************************************************************************
//**********************   IecMaster methods   *********************************
//******************************************************************************

  procedure IecMaster_C.Execute();
  var
    buf     : ApduBuffer_T;
    lastErr : Integer;
  begin
    tcp := TTCPBlockSocket.Create();

    while true do
    begin
      lastErr := tcp.LastError;

      if lastErr <> 0 then
      begin
        tcp.CloseSocket;
        sleep(50);
        tcp.CreateSocket;
        tcp.Connect(address, port);

        OnConnected(0);
      end else
      begin
        tcp.RecvBuffer(@buf, sizeof(buf));
        lastErr := tcp.LastError;

        if (tcp.RecvCounter > 0) and (lastErr = 0) then OnReceive(buf, 0);

      end;

    end;
  end;

  procedure IecMaster_C.Send(buf : ApduBuffer_T; len : Integer);
  const
    maxTry = 100;
  var
    numTry : Integer;
  begin
    if tcp = nil then Exit;

    numTry := 0;

    while tcp.LastError <> 0 do
    begin
      sleep(10);
      inc(numTry);

      if numTry > maxTry then
      begin
        Exit;
      end;

    end;
    tcp.SendBuffer(@buf, len);
  end;

//******************************************************************************
//**********************   IecSlave methods   **********************************
//******************************************************************************

  procedure IecSlave_C.Execute();
  var
    i          : Integer;
  begin
    Queue        := Executor_C.Create(False);
    Queue.OnExec := OnReceive;
    tcp          := TTCPBlockSocket.Create();
    ServerSock   := TTCPBlockSocket.Create();

    ServerSock.Bind('0.0.0.0', port);
    ServerSock.Listen;

    for i := 0 to Length(pa) - 1 do
    begin
      pa[i] := nil;
    end;

    while not Terminated do
    begin

      if ServerSock.CanRead(50) then
      begin
        for i := 0 to Length(Pa) - 1 do
        begin

          if Pa[i] = nil then
          begin
            Pa[i] := ConnectProxy_C.Create(ServerSock.Accept, i, queue);
            OnConnected(i);
            break;
          end;

        end;
      end else
        begin
          for i := 0 to Length(Pa) - 1 do
          begin

            if Pa[i] <> nil then
            begin

              if not pa[i].IsConnectionOpnd then
              begin
                FreeAndNil(Pa[i]);
                Pa[i] := nil;
              end;

            end;

          end;
        end;

    end;
    for i := 0 to Length(Pa) - 1 do
    begin
      if Pa[i] <> nil then
      begin
        pa[i].Terminate;
        pa[i].WaitFor;
        FreeAndNil(Pa[i]);
      end;
    end;
    Queue.Terminate;
    Queue.WaitFor;
    ServerSock.Free;
  end;

  procedure IecSlave_C.Send(buf : ApduBuffer_T; len,id : Integer);
  var
    i : Integer;
  begin

    if id = -1 then // global
    begin
      for i := 0 to Length(pa) - 1 do
      begin
        if Pa[i] <> nil then Pa[i].Send(buf, len);
      end;
    end;

    if Pa[id] = nil then Exit;
    if not Pa[id].IsConnectionOpnd then Exit;

    Pa[id].Send(buf, len);
  end;

//******************************************************************************
//**********************   Executor methods   **********************************
//******************************************************************************

  procedure Executor_C.Execute();
  var
    ep : Integer;
  begin
    CS  := TCriticalSection.Create;
    Cnt := 0;

    while not Terminated do
    begin

      if Cnt > 0 then
      begin

        if Queue[ep].IsUnhandled then
        begin
          OnExec(Queue[ep].Buf, Queue[ep].Id);
          CS.Enter;
          Queue[ep].IsUnhandled := False;
          dec(Cnt);
          CS.Leave;
        end;

        if ep < Length(Queue) then inc(ep)
        else ep := 0;

      end else
      begin
        sleep(10);
        ep := 0;
      end;

    end;
  CS.Free;

  end;

  procedure Executor_C.Enqueue(buf : ApduBuffer_T; id : Integer);
  var
    i       : Integer;
    isFound : Boolean;
  begin

    if Cnt < Length(Queue) then
    begin
      CS.Enter;
      i := 0;
      isFound := False;
      while not isFound do
      begin

        if not Queue[i].IsUnhandled then
        begin
          Queue[i].Buf         := buf;
          Queue[i].Id          := id;
          Queue[i].IsUnhandled := True;
          isFound              := True;
          inc(Cnt);
        end;

        if i < Length(Queue) then inc(i)
        else i := 0;

      end;

      CS.Leave;
    end;

  end;

//******************************************************************************
//**********************   ConnectProxy methods   ******************************
//******************************************************************************

  constructor ConnectProxy_C.Create(Hsock : Integer; connectId : Integer; var execQueue : Executor_C);
  begin
    inherited Create(False);
    tcpConn              := TTCPBlockSocket.Create();
    tcpConn.Socket       := Hsock;
    self.Suspended       := False;
    queue                := execQueue;
    Id                   := connectId;
    IsConnectionOpnd     := True;
    self.FreeOnTerminate := False;
  end;

  procedure ConnectProxy_C.Execute();
  var
    buf        : ApduBuffer_T;
    lastErr    : Integer;
  begin

    lastErr := 0;
    while (lastErr = 0) and (not self.Terminated) do
      begin
        tcpConn.RecvBuffer(@buf, sizeof(buf));
        lastErr := tcpConn.LastError;

        if lastErr = 0 then queue.Enqueue(buf, Id);

      end;

      tcpConn.CloseSocket;
      tcpConn.CreateSocket;
      IsConnectionOpnd := False;
      tcpConn.Free;
  end;

  procedure ConnectProxy_C.Send(buf : ApduBuffer_T; Len : Integer);
  const
    maxTry = 100;
    delayBetweenTrying = 10;
  var
    numTry : Integer;
  begin

    if tcpConn = nil then Exit;

    numTry := 0;
    while tcpConn.LastError <> 0 do
    begin
      sleep(delayBetweenTrying);
      inc(numTry);

      if numTry > maxTry then Exit;

    end;

    tcpConn.SendBuffer(@buf, len);
  end;

end.

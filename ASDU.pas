unit ASDU;

interface
  uses Classes, Tools, ASDUData, InformObj, InformObjConst;

  type

    Asdu101Ser_C = class(TObject)
    public
       ASDU_Dat                       :  ASDU_T;
       ASDU_Len                       : Integer;
       OriginalAddressOctets          : 0..1; // manual setting
       AsudAddresOctets               : 1..2; // manual setting
       ObjectInformationAddressOctets : 1..3; // manual setting

    private
       InfObj                         : InfarmationObject_C;

    public
       Constructor Create();
       procedure GenerateASDU(descr : ApduDescription_R);
       procedure ParseASDU(var ReceiveASDU:  ASDU_T; var descr : ApduDescription_R);

    private
       procedure UpdateASDUHeader(address : Addr_E; value : Byte);
       procedure ClearASDU();
       function GetAddrOffset(address : Addr_E) : Integer;
    end;


implementation

  //****************************************************************************
  //*******************   ASDU101Ser methods   *********************************
  //****************************************************************************

  Constructor Asdu101Ser_C.Create();
  begin
      self.OriginalAddressOctets          := 0;
      self.AsudAddresOctets               := 1;
      self.ObjectInformationAddressOctets := 1;
      self.InfObj                         := InfarmationObject_C.Create();
  end;

//******************************************************************************
//**********************   Generate / Parse    *********************************
//******************************************************************************

  // +-----------------------------------------------+
  // |             TYPE IDENTIFICATION               | OCTET
  // +----+------------------------------------------+
  // | SQ |     VARIABLE STRUCTURE QUALIFER(VSQ)     | OCTET
  // +----+----+-------------------------------------+
  // | T  | PN |        CAUSE OF TRANSMITTION        | OCTET
  // +----+----+-------------------------------------+
  // |              ORIGINATOR ADDRES                | OCTET  ***
  // +-----------------------------------------------+
  // |              ASDU ADDRESS FIELD               | OCTET  ***
  // +-----------------------------------------------+
  // |              ASDU ADDRESS FIELD               | OCTET  ***
  // +-----------------------------------------------+
  // |       INFORMATION OBJECT ADRESS FIELDS        | OCTET
  // +-----------------------------------------------+
  // |       INFORMATION OBJECT ADRESS FIELDS        | OCTET  ***
  // +-----------------------------------------------+
  // |       INFORMATION OBJECT ADRESS FIELDS        | OCTET  ***
  // +-----------------------------------------------+
  // |              OBJECT INFORMATION               | OCTET
  // +-----------------------------------------------+
  // +-----------------------------------------------+        ***
  // +-----------------------------------------------+        ***
  // +-----------------------------------------------+        ***
  // |
  //          "***" -- is optional parameter

  procedure Asdu101Ser_C.GenerateASDU(descr : ApduDescription_R);
  var
    current : Byte;
  begin
    ClearASDU();
    current := Byte(descr.ASDUType);
    UpdateASDUHeader(Addr_E.TYPE_IDENTIFICATION, current);

    current := descr.StrcQual.VSQ;
    if descr.StrcQual.SQ then current := SetBit(current, SelectBit(7));

    UpdateASDUHeader(Addr_E.VARIABLE_STRCTRE_QUALIFIER, current);

    current := Byte(descr.CauseTrans.CauseTrans);
    if descr.CauseTrans.PN then current := SetBit( current, SelectBit(6));
    if descr.CauseTrans.T then current  := SetBit( current, SelectBit(7));

    UpdateASDUHeader(Addr_E.CAUSE_OF_TRANSMISSION, current);

    current := descr.OriginatorAdderess;
    UpdateASDUHeader(Addr_E.ORIGINATOR_ADDRESS, current);

    current := descr.ASDUAddress and $ff;
    UpdateASDUHeader(Addr_E.ASDU_ADDRESS_PATH1, current);

    current := (descr.ASDUAddress and $ff00) shr 8;
    UpdateASDUHeader(Addr_E.ASDU_ADDRESS_PATH2, current);

    // add information object
      InfObj.ObjectInformationAddressOctets := ObjectInformationAddressOctets;
      InfObj.Generate(descr);
      CPArray(ASDU_Dat, InfObj.OI_Dat, InfObj.OI_Len, ASDU_Len, 0);
      ASDU_Len                              := ASDU_Len + InfObj.OI_Len;

  end;

  procedure Asdu101Ser_C.ParseASDU(var ReceiveASDU:  ASDU_T; var descr : ApduDescription_R);
  var
    offset    : integer;
    IOReceive : ObjectInformation_T;
  begin
    ASDU_Dat                    := ReceiveASDU;

    offset                      := GetAddrOffset(Addr_E.TYPE_IDENTIFICATION);
    descr.ASDUType              := AsduTypes_E(ASDU_Dat[offset]);

    offset                      := GetAddrOffset(Addr_E.VARIABLE_STRCTRE_QUALIFIER);
    descr.StrcQual.VSQ          := ASDU_Dat[offset] and $7f;
    descr.StrcQual.SQ           := QueryBit(ASDU_Dat[offset], SelectBit(7));

    offset                      := GetAddrOffset(Addr_E.CAUSE_OF_TRANSMISSION);
    descr.CauseTrans.CauseTrans := CauseOfTrans_E(ASDU_Dat[offset] and $3f);
    descr.CauseTrans.PN         := QueryBit(ASDU_Dat[offset], SelectBit(6));
    descr.CauseTrans.T          := QueryBit(ASDU_Dat[offset], SelectBit(7));

    offset                      := GetAddrOffset(Addr_E.ORIGINATOR_ADDRESS);
    descr.OriginatorAdderess    := 0;

    if offset >= 0  then
    descr.OriginatorAdderess    := ASDU_Dat[offset];

    offset                      := GetAddrOffset(Addr_E.ASDU_ADDRESS_PATH1);
    descr.ASDUAddress           := 0;

    if offset >= 0  then
    descr.ASDUAddress           := ASDU_Dat[offset];

    offset                      := GetAddrOffset(Addr_E.ASDU_ADDRESS_PATH2);

    if offset >= 0  then
     descr.ASDUAddress          := SetBit(descr.ASDUAddress, ASDU_Dat[offset] shl 8);

    offset                      := GetAddrOffset(Addr_E.OBJECT_INFARMATION);
    CPArray(IOReceive, ASDU_Dat, ASDU_Len - offset - 1, 0, offset);
    InfObj.OI_Len               := ASDU_Len - offset - 1;

    infObj.ObjectInformationAddressOctets := ObjectInformationAddressOctets;
    InfObj.Parse(IOReceive, descr);
  end;

//******************************************************************************
//******************************************************************************
//******************************************************************************

  procedure Asdu101Ser_C.UpdateASDUHeader(address : Addr_E; value : Byte);
  var
    adressOffset : Integer;
  begin
    adressOffset := GetAddrOffset(address);

    if adressOffset < 0 then Exit;

    ASDU_Dat[adressOffset] := Byte(value);

    if adressOffset >= ASDU_Len then ASDU_Len := adressOffset + 1;

  end;

  procedure Asdu101Ser_C.ClearASDU();
      var i : Integer;
  begin
    for i := 0 to ASDU_MAX_LEN do
    begin
      ASDU_Dat[i] := 0;
    end;
    ASDU_Len      := 0;
  end;

  function Asdu101Ser_C.GetAddrOffset(address : Addr_E) : Integer;
  var
    offset : Integer;
  begin
    offset := 0;

    // calcalate offset
    if address > Addr_E.ORIGINATOR_ADDRESS then
      offset := offset + self.OriginalAddressOctets - 1;

    if address > Addr_E.ASDU_ADDRESS_PATH2 then
      offset := offset + self.AsudAddresOctets - 2;

    // handled exceptions
    if (address = Addr_E.ORIGINATOR_ADDRESS) and (self.OriginalAddressOctets < 1)  then
      Exit(-1); // Address don't exist

    if (address = Addr_E.ASDU_ADDRESS_PATH2) and (self.AsudAddresOctets < 2)  then
      Exit(-1); // Address don't exist

    Result := Integer(address) + offset;
  end;



end.

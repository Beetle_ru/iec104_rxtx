unit Tools;

interface
  function  SetBit(Src: Integer; Mask: Integer): Integer;
  function  ResetBit(Src: Integer; Mask: Integer): Integer;
  function  InvertBit(Src: Integer; Mask: Integer): Integer;
  function  QueryBit(Val: Integer; Mask: Integer): Boolean;
  function  SelectBit(NumberOfBit: Integer): Integer;
  procedure CPArray(var dest, src : array of Byte; cnt, offsetDest, offsetSrc : Integer);
  function  GetBitField(var field : array of byte; const aIndex: Integer): Int64;
  procedure SetBitField(var field : array of byte; aIndex, aValue: Int64);
  function  GetBitCount(value : LongInt; typeSizeOf : Byte) : Cardinal;

implementation

  function SetBit(Src: Integer; Mask: Integer): Integer;
    begin
      Result := Src or Mask;
  end;

  function ResetBit(Src: Integer; Mask: Integer): Integer;
  begin
    Result := Src and not Mask;
  end;

  function InvertBit(Src: Integer; Mask: Integer): Integer;
  begin
    Result := Src xor Mask;
  end;

  function QueryBit(Val: Integer; Mask: Integer): Boolean;
  Begin
    Result := False;

    if Val and Mask = Mask then Result := True;

    if Val and Mask = 0  then Result := False;

  end;

  function SelectBit(NumberOfBit: Integer): Integer;
  begin
      Result := 1 shl NumberOfBit;
  end;

  procedure CPArray(var dest, src : array of Byte; cnt, offsetDest, offsetSrc : Integer);
    var destI, srcI, i : Integer;
  begin
    for i := 0 to cnt do
    begin
      destI := (i + offsetDest);
      srcI  := (i + offsetSrc);

      if (destI > Length(dest)) or (srcI > Length(src)) then Exit;

      dest[destI] := src[srcI];
    end;
  end;

  //{$OPTIMIZATION ON}
  //{$OVERFLOWCHECKS OFF}
  function GetBitField(var field : array of byte; const aIndex: Integer): Int64;
  var
    Offset        : Word;
    NrBits        : Word;
    LastByte      : Word;
    i             : Word;
    Shift         : Word;
    Mask          : Byte;
    CurrentMusk   : Byte;
    FullMaskBytes : Byte;
    StartByte     : byte;
    CurrentBit    : byte;
  begin
    NrBits        := aIndex and $FF;
    Offset        := aIndex shr 8;
    StartByte     := Offset div 8;
    CurrentBit    := Offset mod 8;
    LastByte      := (NrBits + Offset - 1) div 8;
    FullMaskBytes := NrBits div 8;

    Mask   := ((1 shl (NrBits mod 8)) - 1);

    Result := 0;
    Shift  := 0;

    for i  := StartByte to LastByte do
    begin

      if i >= FullMaskBytes then CurrentMusk := Mask
      else CurrentMusk := $ff;

      Result     := Result or (((field[i] and (CurrentMusk shl CurrentBit)) shr CurrentBit) shl Shift);
      Shift      := Shift + (8 - CurrentBit);
      CurrentBit := 0;
    end;
  end;

  procedure SetBitField(var field : array of byte; aIndex, aValue: Int64);
  var
    Offset        : Word;
    NrBits        : Word;
    LastByte      : Word;
    i             : Word;
    Mask          : Byte;
    CurrentMusk   : Byte;
    FullMaskBytes : Byte;
    StartByte     : byte;
    CurrentBit    : byte;
  begin
    NrBits        := aIndex and $FF;
    Offset        := aIndex shr 8;
    StartByte     := Offset div 8;
    CurrentBit    := Offset mod 8;
    LastByte      := (NrBits + Offset - 1) div 8;
    FullMaskBytes := NrBits div 8;

    Mask := ((1 shl (NrBits mod 8)) - 1);

    Assert((GetBitCount(aValue, sizeof(aValue))  <= (GetBitCount(Mask, sizeof(Mask)) + (FullMaskBytes * 8))), 'too large value');

    for i := StartByte to LastByte do
    begin

      if i >= FullMaskBytes then CurrentMusk := Mask
      else CurrentMusk := $ff;

      field[i]   := (field[i] and (not (CurrentMusk shl CurrentBit))) or (aValue shl CurrentBit);
      aValue     := aValue shr (8 - CurrentBit);
      CurrentBit := 0;
    end;
  end;

  function GetBitCount(value : LongInt; typeSizeOf : Byte) : Cardinal;
  begin
    if value < 0 then
    begin
      Result := typeSizeOf;
      Exit;
    end;
    if value <> 0 then Result := round(ln(value)/ln(2)) + 1
    else Result := 0;
  end;

end.

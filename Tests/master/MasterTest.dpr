program MasterTest;

{$APPTYPE CONSOLE}

uses
  {$IFDEF UNIX} cthreads, {$ENDIF}
  SysUtils,//, Comunicator, ASDUData, InformObjConst;
  Comunicator in '../../Comunicator.pas',
  InformObjConst in '../../InformObjConst.pas',
  ASDUData in '../../ASDUData.pas';

  var
  descr        : ApduDescription_R;
  IEC104Master : IEC104_C;


  procedure ValueHandler(descr : ApduDescription_R; id : Integer);
  var
    i : Integer;
  begin
    writeln('type = ', Integer(descr.ASDUType));
  end;

  function GetCSENC1() : ApduDescription_R;
  var
    descr : ApduDescription_R;
    i     : Integer;
  begin
    Init_ApduDescription(descr);

    descr.ASDUType := AsduTypes_E.C_SE_NC_1;
    descr.StrcQual.VSQ := 10;
    //descr.StrcQual.SQ := false;
    descr.StrcQual.SQ := true;
    descr.CauseTrans.CauseTrans := CauseOfTrans_E.PER_CYC;
    descr.CauseTrans.PN := True;
    descr.CauseTrans.T := True;
    descr.OriginatorAdderess := 12;
    descr.ASDUAddress := $cccc;
    descr.InfObjAddress := $ffffff;
    //descr.InfObjAddress := 111;

    for i := 0 to descr.StrcQual.VSQ - 1 do
    begin
    descr.InfObjDescr[i].InfObjAddress := i;
    descr.InfObjDescr[i].CSENC1.R32IEEE754.Value := 11.33;
    descr.InfObjDescr[i].CSENC1.QOS.QL := 1;
    end;

    Result := descr;
  end;

  function GetMSPNA1() : ApduDescription_R;
  var
    descr : ApduDescription_R;
    i     : Integer;
  begin
    Init_ApduDescription(descr);

    descr.ASDUType := AsduTypes_E.M_SP_NA_1;
    descr.StrcQual.VSQ := 10;
    //descr.StrcQual.SQ := false;
    descr.StrcQual.SQ := true;
    descr.CauseTrans.CauseTrans := CauseOfTrans_E.PER_CYC;
    descr.CauseTrans.PN := False;
    descr.CauseTrans.T := False;
    descr.OriginatorAdderess := 1;
    descr.ASDUAddress := $cccc;
    descr.InfObjAddress := 1;

    for i := 0 to descr.StrcQual.VSQ - 1 do
    begin
      descr.InfObjDescr[i].InfObjAddress := i + descr.InfObjAddress;
      descr.InfObjDescr[i].MSPNA1.SIQ.SPI := 1;
      descr.InfObjDescr[i].MSPNA1.SIQ.BL := 0;
      descr.InfObjDescr[i].MSPNA1.SIQ.SB := 1;
      descr.InfObjDescr[i].MSPNA1.SIQ.NT := 1;
      descr.InfObjDescr[i].MSPNA1.SIQ.IV := 1;
    end;

    Result := descr;
  end;

begin
  try
    IEC104Master := IEC104_C.Create(Direction_T.MASTER, '127.0.0.1', '2404');
    //IEC104Master := IEC104.Create(Direction_T.MASTER, '172.16.10.30', '2404');
    IEC104Master.Iec104Ser_obj.OriginalAddressOctets := 1;
    IEC104Master.Iec104Ser_obj.AsudAddresOctets := 2;
    IEC104Master.Iec104Ser_obj.ObjectInformationAddressOctets := 3;
    IEC104Master.UserValueHandlerPoc := ValueHandler;

    //IEC104Master.RS_Verifyer.Interval:=100;
    //descr := GetCSENC1();

    //writeln(sizeof(descr.InfObjDescr[0]));


    while True do
    begin
      //readln;
      descr := GetMSPNA1();
      IEC104Master.PushSingle(descr, 0);
      sleep(3000);
      //readln;
      //IEC104Master.SARTDT();
    end;


  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.

program SlaveTest;

{$APPTYPE CONSOLE}

uses
  {$IFDEF UNIX} cthreads, {$ENDIF}
  SysUtils,//, Comunicator, ASDUData, InformObjConst;
  Comunicator in '../../Comunicator.pas',
  InformObjConst in '../../InformObjConst.pas',
  ASDUData in '../../ASDUData.pas';


var
  IEC104Slave : IEC104_C;
  descr : ApduDescription_R;
  i : Integer;
  iterator : Timer_C;
  iterators : array[0..10] of Timer_C;
  process : Timer_C;
  cv : Integer;
  av : ActualValues_C;


  procedure Response(value : Integer; id : Integer);
  var
    descr : ApduDescription_R;
    i : Integer;
  begin
    Init_ApduDescription(descr);

    descr.ASDUType := AsduTypes_E.M_SP_TB_1;
    descr.StrcQual.VSQ := 1;
    descr.StrcQual.SQ := false;
    //descr.StrcQual.SQ := true;
    descr.CauseTrans.CauseTrans := CauseOfTrans_E.INROGEN;
    descr.CauseTrans.PN := False;
    descr.CauseTrans.T := False;
    descr.OriginatorAdderess := 0;
    descr.ASDUAddress := 2404;
    //descr.InfObjAddress := 393729;
    descr.InfObjAddress := 1;

    for i := 0 to descr.StrcQual.VSQ - 1 do
    begin
      descr.InfObjDescr[i].InfObjAddress := i + descr.InfObjAddress;
      descr.InfObjDescr[i].MSPTB1.SIQ.SPI := value;
      descr.InfObjDescr[i].MSPTB1.SIQ.BL := 1;
      descr.InfObjDescr[i].MSPTB1.SIQ.SB := 1;
      descr.InfObjDescr[i].MSPTB1.SIQ.NT := 1;
      descr.InfObjDescr[i].MSPTB1.SIQ.IV := 1;

      descr.InfObjDescr[i].MSPTB1.Time.MilliSeconds := 1000;
      descr.InfObjDescr[i].MSPTB1.Time.Minutes := 10;
      descr.InfObjDescr[i].MSPTB1.Time.Hours := 2;
      descr.InfObjDescr[i].MSPTB1.Time.DayOfMonth := 1;
      descr.InfObjDescr[i].MSPTB1.Time.DayOfWeek := 1;
      descr.InfObjDescr[i].MSPTB1.Time.Months := 1;
      descr.InfObjDescr[i].MSPTB1.Time.Years := 4;
    end;

    IEC104Slave.PushSingle(descr, id);
    write('************** id = ', id,' ******************');

  end;

  procedure ValueHandler(descr : ApduDescription_R; id : Integer);
  var
    i : Integer;
  begin
    writeln('@@@@@@@@@@@@@ type = ', Integer(descr.ASDUType));
    //IEC104Slave.Response(descr, id);

    if descr.ASDUType = AsduTypes_E.C_IC_NA_1 then
    begin
      writeln('C_IC_NA_1.CICNA1.QOI.Pointer = ', descr.InfObjDescr[0].CICNA1.QOI.Pointer);
      //Response();
      writeln('transmition... id = ', id);
      //iterator.StartTimer;
      iterators[id].StartTimer;
    end;

    //Response(1, id);

  end;

  procedure IteratorHandler(value : TObject);
  begin
    Response(cv, (value as ActualValues_C).Id);
    //writeln('__cv = ', cv);
  end;

  procedure ProcessHandler(value : TObject);
  begin
    if cv = 0 then cv := 1
    else cv := 0;
    //writeln('cv = ', cv);
  end;


begin
  try
    IEC104Slave := IEC104_C.Create(Direction_T.SLAVE, '127.0.0.1', '2404');
    IEC104Slave.Iec104Ser_obj.OriginalAddressOctets := 1;
    IEC104Slave.Iec104Ser_obj.AsudAddresOctets := 2;
    IEC104Slave.Iec104Ser_obj.ObjectInformationAddressOctets := 3;

    IEC104Slave.UserValueHandlerPoc := ValueHandler;

    process := Timer_C.Create(True);
    process.HandlerTimerProc := ProcessHandler;
    process.Interval := 3000;
    process.StartTimer;


    for i := 0 to 10 do
    begin
      iterators[i]                  := Timer_C.Create(true);
      iterators[i].HandlerTimerProc := IteratorHandler;
      iterators[i].Interval         := 500;
      av                            :=  ActualValues_C.Create;
      av.Id                         := i;
      iterators[i].value            := av;
    end;

    cv := 0;

    //readln;
    //writeln('transmition...');
    //response();
    //iterator.StartTimer;
    readln;
    //IEC104Slave.SlaveCloseConnection(-1);

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.

unit IecSerializer;

interface

  uses
    Classes, ASDU, ASDUData, Tools;

  const
    // Protocol constants
    APCI_START       = $68;
    APCI_LEN         = 6;
    APCI_START_LEN   = 2;
    APCI_DATA_LEN	   = (APCI_LEN - APCI_START_LEN);
    APDU_MIN_LEN     = 4;
    APDU_MAX_LEN     = 253;
    BUFFER_SIZE      = 255;

  type
    // Constants relative to the filed, independent of the field position in the byte
    // U (Unnombered) constants
    UCommands_E = (
      U_STARTDT_ACT  = $4,
      U_STARTDT_CON	 = $8,
      U_STOPDT_ACT 	 = $10,
      U_STOPDT_CON	 = $20,
      U_TESTFR_ACT 	 = $40,
      U_TESTFR_CON	 = $80
    );

    // APCI types
    AcpiTypes_E = (
      I_TYPE	          =	0,
      S_TYPE	          =	1,
      U_TYPE	          =	3,
      APCI_TYPE_UNKNOWN = 4
    );

    // addresation
    Addr_E = (
      START      = 0,
      LEN        = 1,
      B1         = 2,
      B2         = 3,
      B3         = 4,
      B4         = 5,
      BEGIN_ASDU = 6
    );

    // short name for get integer array index
    Indx = Integer;

    // APDU data type
    ApduBuffer_T = array[0..BUFFER_SIZE] of byte;

    Iec104Ser_C = class(TObject)
    public
      APDU_Dat       : ApduBuffer_T;
      APDU_Len       : Integer;
      U_Handler : procedure (operation : UCommands_E; id : Integer) of object;
      I_Handler : procedure (descr : ApduDescription_R; id : Integer) of object;
      S_Handler : procedure (sequenceNumberR : Integer; id : Integer) of object;
      OriginalAddressOctets          : 0..1; // manual setting
      AsudAddresOctets               : 1..2; // manual setting
      ObjectInformationAddressOctets : 1..3; // manual setting

    private
      Asdu101Ser : Asdu101Ser_C;

    public
      Constructor Create();
      procedure Parse(var APDU_Receive : ApduBuffer_T; id : Integer);
      procedure GenerateU(operation : UCommands_E);
      procedure GenerateI(descr : ApduDescription_R);
      procedure GenerateS(sequenceNumberR : Integer);

    private
      procedure ParseU(id : Integer);
      procedure ParseI(id : Integer);
      procedure ParseS(id : Integer);
      procedure ClearAPDU();

    end;

implementation

//******************************************************************************
//**********************   Iec104Ser methods   *********************************
//******************************************************************************

  Constructor Iec104Ser_C.Create();
  begin
    // Create and settings ASDU 101 serializer
    Asdu101Ser                     := Asdu101Ser_C.Create();
    // default settings
    OriginalAddressOctets          := 1;
    AsudAddresOctets               := 2;
    ObjectInformationAddressOctets := 3;
  end;

//******************************************************************************
//**********************   Gnerate / Parse U   *********************************
//******************************************************************************

  // +-----------------------------------------------+
  // |                    START                      | 1 OCTET
  // +-----------------------------------------------+
  // |                    LENGTH                     | 2 OCTET
  // +-----------+-----------+-----------+-----+-----+
  // |   TESTFR  |   STOPDT  |  STARTDT  |     |     |
  // +-----+-----+-----+-----+-----+-----+  1  |  1  | 3 OCTET
  // | con | act | con | act | con | act |     |     |
  // +-----+-----+-----+-----+-----+-----+-----+-----+
  // |                      0                        | 4 OCTET
  // +-----------------------------------------------+
  // |                      0                        | 5 OCTET
  // +-----------------------------------------------+
  // |                      0                        | 6 OCTET
  // +-----------------------------------------------+

  procedure Iec104Ser_C.GenerateU(operation : UCommands_E);
  begin
    ClearAPDU;

    APDU_Dat[Indx(Addr_E.START)] := APCI_START;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.LEN)]   := APDU_MIN_LEN;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.B1)]    := Byte(AcpiTypes_E.U_TYPE);
    APDU_Dat[Indx(Addr_E.B1)]    := SetBit(APDU_Dat[Indx(Addr_E.B1)], Integer(operation));
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.B2)]    := 0;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.B3)]    := 0;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.B4)]    := 0;
    inc(APDU_Len);
  end;

  procedure Iec104Ser_C.ParseU(id : Integer);
  var
    currentB : byte;
    i        : integer;
    flag     : boolean;
  begin
    currentB := APDU_Dat[Indx(Addr_E.B1)];
    flag     := false;

    for i := 2 to 7 do
    begin

      if ((currentB and $fc) = (1 shl i)) and flag then Assert(false, 'unknown opeation');
      if (currentB and $fc) = (1 shl i) then flag := true;

    end;

    if flag then begin
      Assert(not(@U_Handler = nil), 'U unhandled, please add handler');
      U_Handler(UCommands_E((currentB and $fc)), id);
    end;

  end;

//******************************************************************************
//**********************   Generate / Parse I   ********************************
//******************************************************************************
  // +-----------------------------------------------+
  // |                    START                      | 1 OCTET
  // +-----------------------------------------------+
  // |                    LENGTH                     | 2 OCTET
  // +-----------------------------------------+-----+
  // |        SEND SEQUENCE NUMBER N(S)   LSB  |  0  | 3 OCTET
  // +-----------------------------------------+-----+
  // |  MSB   SEND SEQUENCE NUMBER N(S)              | 4 OCTET
  // +-----------------------------------------+-----+
  // |        SEND SEQUENCE NUMBER N(R)   LSB  |  0  | 5 OCTET
  // +-----------------------------------------+-----+
  // |  MSB   SEND SEQUENCE NUMBER N(R)              | 6 OCTET
  // +-----------------------------------------------+
  // |                   ASDU ....                   | NEXT 251 OCTES

  procedure Iec104Ser_C.GenerateI(descr : ApduDescription_R);
  var
    sn : Integer;
  begin
    ClearAPDU;

    APDU_Dat[Indx(Addr_E.START)] := APCI_START;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.LEN)]   := APDU_MIN_LEN;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.B1)]    := Byte(AcpiTypes_E.I_TYPE);

    sn := descr.Iec104.SequenceNumberS and $00ff;
    APDU_Dat[Indx(Addr_E.B1)]    := SetBit(APDU_Dat[Indx(Addr_E.B1)], (sn shl 1));
    inc(APDU_Len);
    sn := descr.Iec104.SequenceNumberS and $7f80;
    APDU_Dat[Indx(Addr_E.B2)]    := (sn shr 7);
    inc(APDU_Len);

    sn := descr.Iec104.SequenceNumberR and $00ff;
    APDU_Dat[Indx(Addr_E.B3)]    := SetBit(APDU_Dat[Indx(Addr_E.B3)], (sn shl 1));
    inc(APDU_Len);
    sn := descr.Iec104.SequenceNumberR and $7f80;
    APDU_Dat[Indx(Addr_E.B4)]    := (sn shr 7);
    inc(APDU_Len);

    // ASDU add ...

    Asdu101Ser.OriginalAddressOctets          := OriginalAddressOctets;
    Asdu101Ser.AsudAddresOctets               := AsudAddresOctets;
    Asdu101Ser.ObjectInformationAddressOctets := ObjectInformationAddressOctets;
    Asdu101Ser.GenerateASDU(descr);

    CPArray(APDU_Dat, Asdu101Ser.ASDU_Dat, Asdu101Ser.ASDU_Len, APDU_Len, 0);
    APDU_Len := APDU_Len + Asdu101Ser.ASDU_Len;
    APDU_Dat[Indx(Addr_E.LEN)] := APDU_Len - APCI_START_LEN;
  end;

  procedure Iec104Ser_C.ParseI(id : Integer);
  var
    descr : ApduDescription_R;
    ReceiveASDU:  ASDU_T;
  begin
    Init_ApduDescription(descr);

    descr.Iec104.SequenceNumberS := APDU_Dat[Indx(Addr_E.B1)] shr 1;
    descr.Iec104.SequenceNumberS := SetBit(descr.Iec104.SequenceNumberS, APDU_Dat[Indx(Addr_E.B2)] shl 7);

    descr.Iec104.SequenceNumberR := APDU_Dat[Indx(Addr_E.B3)] shr 1;
    descr.Iec104.SequenceNumberR := SetBit(descr.Iec104.SequenceNumberR, APDU_Dat[Indx(Addr_E.B4)] shl 7);

    CPArray(ReceiveASDU, APDU_Dat, APDU_Len - APCI_DATA_LEN, 0, APCI_LEN);

    Asdu101Ser.OriginalAddressOctets          := OriginalAddressOctets;
    Asdu101Ser.AsudAddresOctets               := AsudAddresOctets;
    Asdu101Ser.ObjectInformationAddressOctets := ObjectInformationAddressOctets;

    Asdu101Ser.ASDU_Len := APDU_Len - APCI_DATA_LEN;
    Asdu101Ser.ParseASDU(ReceiveASDU, descr);

    Assert(not(@I_Handler = nil), 'I unhandled, please add handler');
    I_Handler(descr, id);
  end;


//******************************************************************************
//**********************   Generate / Parse S   ********************************
//******************************************************************************
  // +-----------------------------------------------+
  // |                    START                      | 1 OCTET
  // +-----------------------------------------------+
  // |                    LENGTH                     | 2 OCTET
  // +------------------------------------+----+-----+
  // |                                    | 0  |  0  | 3 OCTET
  // +------------------------------------+----+-----+
  // |                                               | 4 OCTET
  // +-----------------------------------------+-----+
  // |        SEND SEQUENCE NUMBER N(R)   LSB  |  0  | 5 OCTET
  // +-----------------------------------------+-----+
  // |  MSB   SEND SEQUENCE NUMBER N(R)              | 6 OCTET
  // +-----------------------------------------------+


  procedure Iec104Ser_C.GenerateS(sequenceNumberR : Integer);
  var
    sn : Integer;
  begin
    ClearAPDU;

    APDU_Dat[Indx(Addr_E.START)] := APCI_START;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.LEN)]   := APDU_MIN_LEN;
    inc(APDU_Len);
    APDU_Dat[Indx(Addr_E.B1)]    := Byte(AcpiTypes_E.S_TYPE);
    inc(APDU_Len);

    APDU_Dat[Indx(Addr_E.B2)]    := 0;
    inc(APDU_Len);

    sn := sequenceNumberR and $00ff;
    APDU_Dat[Indx(Addr_E.B3)]    := SetBit(APDU_Dat[Indx(Addr_E.B3)], (sn shl 1));
    inc(APDU_Len);
    sn := sequenceNumberR and $7f80;
    APDU_Dat[Indx(Addr_E.B4)]    := (sn shr 7);
    inc(APDU_Len);
  end;

  procedure Iec104Ser_C.ParseS(id : Integer);
  var
    sequenceNumberR : Integer;
  begin

    sequenceNumberR := APDU_Dat[Indx(Addr_E.B3)] shr 1;
    sequenceNumberR := SetBit(sequenceNumberR, APDU_Dat[Indx(Addr_E.B4)] shl 7);

    Assert(not(@S_Handler = nil), 'I unhandled, please add handler');
    S_Handler(sequenceNumberR, id);
  end;


//******************************************************************************
//******************************************************************************
//******************************************************************************

  procedure Iec104Ser_C.Parse(var APDU_Receive : ApduBuffer_T; id : Integer);
  var
    currentB : byte;
  begin
    ClearAPDU();
    APDU_Dat := APDU_Receive;

    currentB := APDU_Dat[Indx(Addr_E.START)];
    Assert((currentB = APCI_START), 'is not iec104');

    APDU_Len := APDU_Dat[Indx(Addr_E.LEN)];

    currentB := APDU_Dat[Indx(Addr_E.B1)];
    if (currentB and SelectBit(0)) = 0 then ParseI(id)
    else if (currentB and 3) = Byte(AcpiTypes_E.S_TYPE) then ParseS(id)
    else if (currentB and 3) = Byte(AcpiTypes_E.U_TYPE) then ParseU(id)
    else Assert(false, 'unknown type');
  end;

  procedure Iec104Ser_C.ClearAPDU();
    var i : Integer;
  begin
    for i := 0 to APDU_MAX_LEN do
    begin
      APDU_Dat[i] := 0;
    end;
    APDU_Len := 0;
  end;

end.

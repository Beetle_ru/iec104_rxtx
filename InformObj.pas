unit InformObj;

interface
  uses Classes, Tools, ASDUData, InformObjConst, SysUtils;

  function GetIOLen(TypeAsdu : AsduTypes_E) : Integer;

  type

    InfarmationObject_C = class(TObject)
    public
       OI_Dat                         : ObjectInformation_T;
       OI_Len                         : Integer;
       ObjectInformationAddressOctets : 1..3;        // manual setting


    public
       procedure Generate(var descr : ApduDescription_R);
       procedure Parse(var IOReceive : ObjectInformation_T; var descr : ApduDescription_R);

    private
       procedure GenerateSingle(var param : InfObjDescr_R; typeAsdu : AsduTypes_E);
       procedure GenerateArray(var descr : ApduDescription_R);
       procedure ParseSingle(offset : integer; var param : InfObjDescr_R; typeAsdu : AsduTypes_E);
       procedure ParseArray(var descr : ApduDescription_R);
       procedure AddIOAddress(InfObjAddress : Integer);
       procedure AddByte(data : Byte);
       procedure AddAnyBytes(data : Array of Byte);
       procedure ClearOI();
       function  ParseIOAddress(offset : integer) : Integer;
    end;

implementation
//******************************************************************************
//*************   InfarmationObject methods   **********************************
//******************************************************************************

//******************************************************************************
//***************************   Generate   *************************************
//******************************************************************************

  procedure InfarmationObject_C.Generate(var descr : ApduDescription_R);
  var
    i : integer;
  begin
    ClearOI();

    if descr.StrcQual.SQ then
    begin
      AddIOAddress(descr.InfObjAddress);
      for i := 0 to descr.StrcQual.VSQ - 1 do GenerateSingle(descr.InfObjDescr[i], descr.ASDUType);
    end else GenerateArray(descr);

  end;

  procedure InfarmationObject_C.GenerateSingle(var param : InfObjDescr_R; typeAsdu : AsduTypes_E);
  begin
    case typeAsdu  of

      AsduTypes_E.M_SP_NA_1 :
      begin
        AddAnyBytes(param.MSPNA1.SIQ.BitField);
      end;

      AsduTypes_E.M_SP_TA_1 :
      begin
        AddAnyBytes(param.MSPTA1.SIQ.BitField);
        AddAnyBytes(param.MSPTA1.Time.BitField);
      end;

      AsduTypes_E.M_DP_NA_1 :
      begin
        AddAnyBytes(param.MDPNA1.DIQ.BitField);
      end;

      AsduTypes_E.M_DP_TA_1 :
      begin
        AddAnyBytes(param.MDPTA1.DIQ.BitField);
        AddAnyBytes(param.MDPTA1.Time.BitField);
      end;

      AsduTypes_E.M_ST_NA_1 :
      begin
        AddAnyBytes(param.MSTNA1.VTI.BitField);
        AddAnyBytes(param.MSTNA1.QDS.BitField);
      end;

      AsduTypes_E.M_ST_TA_1 :
      begin
        AddAnyBytes(param.MSTTA1.VTI.BitField);
        AddAnyBytes(param.MSTTA1.QDS.BitField);
        AddAnyBytes(param.MSTTA1.Time.BitField);
      end;

      AsduTypes_E.M_BO_NA_1 :
      begin
        AddAnyBytes(param.MBONA1.BSI.BitField);
        AddAnyBytes(param.MBONA1.QDS.BitField);
      end;

      AsduTypes_E.M_BO_TA_1 :
      begin
        AddAnyBytes(param.MBOTA1.BSI.BitField);
        AddAnyBytes(param.MBOTA1.QDS.BitField);
        AddAnyBytes(param.MBOTA1.Time.BitField);
      end;

      AsduTypes_E.M_ME_NA_1 :
      begin
        AddAnyBytes(param.MMENA1.NVA.BitField);
        AddAnyBytes(param.MMENA1.QDS.BitField);
      end;

      AsduTypes_E.M_ME_TA_1 :
      begin
        AddAnyBytes(param.MMETA1.NVA.BitField);
        AddAnyBytes(param.MMETA1.QDS.BitField);
        AddAnyBytes(param.MMETA1.Time.BitField);
      end;

      AsduTypes_E.M_ME_NB_1 :
      begin
        AddAnyBytes(param.MMENB1.SVA.BitField);
        AddAnyBytes(param.MMENB1.QDS.BitField);
      end;

      AsduTypes_E.M_ME_TB_1 :
      begin
        AddAnyBytes(param.MMETB1.SVA.BitField);
        AddAnyBytes(param.MMETB1.QDS.BitField);
        AddAnyBytes(param.MMETB1.Time.BitField);
      end;

      AsduTypes_E.M_ME_NC_1 :
      begin
        AddAnyBytes(param.MMENC1.R32IEEE754.BitField);
        AddAnyBytes(param.MMENC1.QDS.BitField);
      end;

      AsduTypes_E.M_ME_TC_1 :
      begin
        AddAnyBytes(param.MMETC1.R32IEEE754.BitField);
        AddAnyBytes(param.MMETC1.QDS.BitField);
        AddAnyBytes(param.MMETC1.Time.BitField);
      end;

      AsduTypes_E.M_IT_NA_1 :
      begin
        AddAnyBytes(param.MITNA1.BCR.BitField);
      end;

      AsduTypes_E.M_IT_TA_1 :
      begin
        AddAnyBytes(param.MITTA1.BCR.BitField);
        AddAnyBytes(param.MITTA1.Time.BitField);
      end;

      AsduTypes_E.M_EP_TA_1 :
      begin
        AddAnyBytes(param.MEPTA1.SEP.BitField);
        AddAnyBytes(param.MEPTA1.Interval.BitField);
        AddAnyBytes(param.MEPTA1.Time.BitField);
      end;

      AsduTypes_E.M_EP_TB_1 :
      begin
        AddAnyBytes(param.MEPTB1.SPE.BitField);
        AddAnyBytes(param.MEPTB1.QDP.BitField);
        AddAnyBytes(param.MEPTB1.Interval.BitField);
        AddAnyBytes(param.MEPTB1.Time.BitField);
      end;

      AsduTypes_E.M_EP_TC_1 :
      begin
        AddAnyBytes(param.MEPTC1.OCI.BitField);
        AddAnyBytes(param.MEPTC1.QDP.BitField);
        AddAnyBytes(param.MEPTC1.Interval.BitField);
        AddAnyBytes(param.MEPTC1.Time.BitField);
      end;

      AsduTypes_E.M_PS_NA_1 :
      begin
        AddAnyBytes(param.MPSNA1.SCD.BitField);
        AddAnyBytes(param.MPSNA1.QDS.BitField);
      end;

      AsduTypes_E.M_ME_ND_1 :
      begin
        AddAnyBytes(param.MMEND1.NVA.BitField);
      end;

      AsduTypes_E.M_SP_TB_1 :
      begin
        AddAnyBytes(param.MSPTB1.SIQ.BitField);
        AddAnyBytes(param.MSPTB1.Time.BitField);
      end;

      AsduTypes_E.M_DP_TB_1 :
      begin
        AddAnyBytes(param.MDPTB1.DIQ.BitField);
        AddAnyBytes(param.MDPTB1.Time.BitField);
      end;

      AsduTypes_E.M_ST_TB_1 :
      begin
        AddAnyBytes(param.MSTTB1.VTI.BitField);
        AddAnyBytes(param.MSTTB1.QDS.BitField);
        AddAnyBytes(param.MSTTB1.Time.BitField);
      end;

      AsduTypes_E.M_BO_TB_1 :
      begin
        AddAnyBytes(param.MBOTB1.BSI.BitField);
        AddAnyBytes(param.MBOTB1.QDS.BitField);
        AddAnyBytes(param.MBOTB1.Time.BitField);
      end;

      AsduTypes_E.M_ME_TD_1 :
      begin
        AddAnyBytes(param.MMETD1.NVA.BitField);
        AddAnyBytes(param.MMETD1.QDS.BitField);
        AddAnyBytes(param.MMETD1.Time.BitField);
      end;

      AsduTypes_E .M_ME_TE_1 :
      begin
        AddAnyBytes(param.MMETE1.SVA.BitField);
        AddAnyBytes(param.MMETE1.QDS.BitField);
        AddAnyBytes(param.MMETE1.Time.BitField);
      end;

      AsduTypes_E.M_ME_TF_1 :
      begin
        AddAnyBytes(param.MMETF1.R32IEEE754.BitField);
        AddAnyBytes(param.MMETF1.QDS.BitField);
        AddAnyBytes(param.MMETF1.Time.BitField);
      end;

      AsduTypes_E.M_IT_TB_1 :
      begin
        AddAnyBytes(param.MITTB1.BCR.BitField);
        AddAnyBytes(param.MITTB1.Time.BitField);
      end;

      AsduTypes_E.M_EP_TD_1 :
      begin
        AddAnyBytes(param.MEPTD1.SEP.BitField);
        AddAnyBytes(param.MEPTD1.Interval.BitField);
        AddAnyBytes(param.MEPTD1.Time.BitField);
      end;

      AsduTypes_E.M_EP_TE_1 :
      begin
        AddAnyBytes(param.MEPTE1.SPE.BitField);
        AddAnyBytes(param.MEPTE1.QDP.BitField);
        AddAnyBytes(param.MEPTE1.Interval.BitField);
        AddAnyBytes(param.MEPTE1.Time.BitField);
      end;

      AsduTypes_E.M_EP_TF_1 :
      begin
        AddAnyBytes(param.MEPTF1.OCI.BitField);
        AddAnyBytes(param.MEPTF1.QDP.BitField);
        AddAnyBytes(param.MEPTF1.Interval.BitField);
        AddAnyBytes(param.MEPTF1.Time.BitField);
      end;

      AsduTypes_E.C_SC_NA_1 :
      begin
        AddAnyBytes(param.CSCNA1.SCO.BitField);
      end;

      AsduTypes_E.C_DC_NA_1 :
      begin
        AddAnyBytes(param.CDCNA1.DCO.BitField);
      end;

      AsduTypes_E.C_RC_NA_1 :
      begin
        AddAnyBytes(param.CRCNA1.RCO.BitField);
      end;

      AsduTypes_E.C_SE_NA_1 :
      begin
        AddAnyBytes(param.CSENA1.NVA.BitField);
        AddAnyBytes(param.CSENA1.QOS.BitField);
      end;

      AsduTypes_E.C_SE_NB_1 :
      begin
        AddAnyBytes(param.CSENB1.SVA.BitField);
        AddAnyBytes(param.CSENB1.QOS.BitField);
      end;

      AsduTypes_E.C_SE_NC_1 :
      begin
        AddAnyBytes(param.CSENC1.R32IEEE754.BitField);
        AddAnyBytes(param.CSENC1.QOS.BitField);
      end;

      AsduTypes_E.C_BO_NA_1 :
      begin
        AddAnyBytes(param.CBONA1.BSI.BitField);
      end;

      AsduTypes_E.M_EI_NA_1 :
      begin
        AddAnyBytes(param.MEINA1.COI.BitField);
      end;

      AsduTypes_E.C_IC_NA_1 :
      begin
        AddAnyBytes(param.CICNA1.QOI.BitField);
      end;

      AsduTypes_E.C_CI_NA_1 :
      begin
        AddAnyBytes(param.CCINA1.QCC.BitField);
      end;

      AsduTypes_E.C_RD_NA_1 :
      begin
        //AddAnyBytes(param.CRDNA1..BitField);
      end;

      AsduTypes_E.C_CS_NA_1 :
      begin
        AddAnyBytes(param.CCSNA1.Time.BitField);
      end;

      AsduTypes_E.C_TS_NA_1 :
      begin
        AddAnyBytes(param.CTSNA1.FBP.BitField);
      end;

      AsduTypes_E.C_RP_NA_1 :
      begin
        AddAnyBytes(param.CRPNA1.QRP.BitField);
      end;

      AsduTypes_E.C_CD_NA_1 :
      begin
        AddAnyBytes(param.CCDNA1.Interval.BitField);
      end;

      AsduTypes_E.P_ME_NA_1 :
      begin
        AddAnyBytes(param.PMENA1.NVA.BitField);
        AddAnyBytes(param.PMENA1.QPM.BitField);
      end;

      AsduTypes_E.P_ME_NB_1 :
      begin
        AddAnyBytes(param.PMENB1.SVA.BitField);
        AddAnyBytes(param.PMENB1.QPM.BitField);
      end;

      AsduTypes_E.P_ME_NC_1 :
      begin
        AddAnyBytes(param.PMENC1.R32IEEE754.BitField);
        AddAnyBytes(param.PMENC1.QPM.BitField);
      end;

      AsduTypes_E.P_AC_NA_1 :
      begin
        AddAnyBytes(param.PACNA1.QPA.BitField);
      end;

      AsduTypes_E.F_FR_NA_1 :
      begin
        AddAnyBytes(param.FFRNA1.NOF.BitField);
        AddAnyBytes(param.FFRNA1.LOF.BitField);
        AddAnyBytes(param.FFRNA1.FRQ.BitField);
      end;

      AsduTypes_E.F_SR_NA_1 :
      begin
        AddAnyBytes(param.FSRNA1.NOF.BitField);
        AddAnyBytes(param.FSRNA1.NOS.BitField);
        AddAnyBytes(param.FSRNA1.LOF.BitField);
        AddAnyBytes(param.FSRNA1.SRQ.BitField);
      end;

      AsduTypes_E.F_SC_NA_1 :
      begin
        AddAnyBytes(param.FSCNA1.NOF.BitField);
        AddAnyBytes(param.FSCNA1.NOS.BitField);
        AddAnyBytes(param.FSCNA1.SCQ.BitField);
      end;

      AsduTypes_E.F_LS_NA_1 :
      begin
        AddAnyBytes(param.FLSNA1.NOF.BitField);
        AddAnyBytes(param.FLSNA1.NOS.BitField);
        AddAnyBytes(param.FLSNA1.LSQ.BitField);
        AddAnyBytes(param.FLSNA1.CHS.BitField);
      end;

      AsduTypes_E.F_AF_NA_1 :
      begin
        AddAnyBytes(param.FAFNA1.NOF.BitField);
        AddAnyBytes(param.FAFNA1.NOS.BitField);
        AddAnyBytes(param.FAFNA1.AFQ.BitField);
      end;

      AsduTypes_E.F_SG_NA_1 :
      begin
        AddAnyBytes(param.FSGNA1.NOF.BitField);
        AddAnyBytes(param.FSGNA1.NOS.BitField);
        AddAnyBytes(param.FSGNA1.LOS.BitField);
      end;

      AsduTypes_E.F_DR_TA_1 :
      begin
        AddAnyBytes(param.FDRTA1.NOF.BitField);
        AddAnyBytes(param.FDRTA1.LOF.BitField);
        AddAnyBytes(param.FDRTA1.SOF.BitField);
        AddAnyBytes(param.FDRTA1.Time.BitField);
      end;

        else
        begin
          OI_Len := -1;
          Exit;
        end;
    end;
  end;

  procedure InfarmationObject_C.GenerateArray(var descr : ApduDescription_R);
  var
    i : integer;
  begin
    for i := 0 to descr.StrcQual.VSQ - 1 do
    begin
      AddIOAddress(descr.InfObjDescr[i].InfObjAddress);
      GenerateSingle(descr.InfObjDescr[i], descr.ASDUType);
    end;
  end;

//******************************************************************************
//***************************   Parse   ****************************************
//******************************************************************************

  procedure InfarmationObject_C.Parse(var IOReceive : ObjectInformation_T; var descr : ApduDescription_R);
  var
    i, offset, itemLen : integer;
  begin
    self.OI_Dat := IOReceive;

    if descr.StrcQual.SQ then
    begin
      itemLen             := GetIOLen(descr.ASDUType);
      offset              := 0;
      descr.InfObjAddress := ParseIOAddress(offset);
      offset              := offset + self.ObjectInformationAddressOctets;

      for i := 0 to descr.StrcQual.VSQ do
      begin
        ParseSingle(offset + (itemLen * i), descr.InfObjDescr[i], descr.ASDUType);
        descr.InfObjDescr[i].InfObjAddress := descr.InfObjAddress + i;
      end;
    end else ParseArray(descr);

  end;

  procedure InfarmationObject_C.ParseSingle(offset : integer; var param : InfObjDescr_R; typeAsdu : AsduTypes_E);
  var
    arrLen : integer;
  begin
    case typeAsdu  of

      AsduTypes_E.M_SP_NA_1 :
      begin
        arrLen := Length(param.MSPNA1.SIQ.BitField);
        CPArray(param.MSPNA1.SIQ.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_SP_TA_1 :
      begin
        arrLen := Length(param.MSPNA1.SIQ.BitField);
        CPArray(param.MSPTA1.SIQ.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MSPTA1.Time.BitField);
        CPArray(param.MSPTA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_DP_NA_1 :
      begin
        arrLen := Length(param.MDPNA1.DIQ.BitField);
        CPArray(param.MDPNA1.DIQ.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_DP_TA_1 :
      begin
        arrLen := Length(param.MDPTA1.DIQ.BitField);
        CPArray(param.MDPTA1.DIQ.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MDPTA1.Time.BitField);
        CPArray(param.MDPTA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ST_NA_1 :
      begin
        arrLen := Length(param.MSTNA1.VTI.BitField);
        CPArray(param.MSTNA1.VTI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MSTNA1.QDS.BitField);
        CPArray(param.MSTNA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ST_TA_1 :
      begin
        arrLen := Length(param.MSTTA1.VTI.BitField);
        CPArray(param.MSTTA1.VTI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MSTTA1.QDS.BitField);
        CPArray(param.MSTTA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MSTTA1.QDS.BitField);
        CPArray(param.MSTTA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_BO_NA_1 :
      begin
        arrLen := Length(param.MBONA1.BSI.BitField);
        CPArray(param.MBONA1.BSI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MBONA1.QDS.BitField);
        CPArray(param.MBONA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_BO_TA_1 :
      begin
        arrLen := Length(param.MBOTA1.BSI.BitField);
        CPArray(param.MBOTA1.BSI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MBOTA1.QDS.BitField);
        CPArray(param.MBOTA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MBOTA1.Time.BitField);
        CPArray(param.MBOTA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_NA_1 :
      begin
        arrLen := Length(param.MMENA1.NVA.BitField);
        CPArray(param.MMENA1.NVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMENA1.QDS.BitField);
        CPArray(param.MMENA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_TA_1 :
      begin
        arrLen := Length(param.MMETA1.NVA.BitField);
        CPArray(param.MMETA1.NVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETA1.QDS.BitField);
        CPArray(param.MMETA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETA1.Time.BitField);
        CPArray(param.MMETA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_NB_1 :
      begin
        arrLen := Length(param.MMENB1.SVA.BitField);
        CPArray(param.MMENB1.SVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMENB1.QDS.BitField);
        CPArray(param.MMENB1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_TB_1 :
      begin
        arrLen := Length(param.MMETB1.SVA.BitField);
        CPArray(param.MMETB1.SVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETB1.QDS.BitField);
        CPArray(param.MMETB1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETB1.Time.BitField);
        CPArray(param.MMETB1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_NC_1 :
      begin
        arrLen := Length(param.MMENC1.R32IEEE754.BitField);
        CPArray(param.MMENC1.R32IEEE754.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMENC1.QDS.BitField);
        CPArray(param.MMENC1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_TC_1 :
      begin
        arrLen := Length(param.MMETC1.R32IEEE754.BitField);
        CPArray(param.MMETC1.R32IEEE754.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETC1.QDS.BitField);
        CPArray(param.MMETC1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETC1.Time.BitField);
        CPArray(param.MMETC1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_IT_NA_1 :
      begin
        arrLen := Length(param.MITNA1.BCR.BitField);
        CPArray(param.MITNA1.BCR.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_IT_TA_1 :
      begin
        arrLen := Length(param.MITTA1.BCR.BitField);
        CPArray(param.MITTA1.BCR.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MITTA1.Time.BitField);
        CPArray(param.MITTA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_EP_TA_1 :
      begin
        arrLen := Length(param.MEPTA1.SEP.BitField);
        CPArray(param.MEPTA1.SEP.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTA1.Interval.BitField);
        CPArray(param.MEPTA1.Interval.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTA1.Time.BitField);
        CPArray(param.MEPTA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_EP_TB_1 :
      begin
        arrLen := Length(param.MEPTB1.SPE.BitField);
        CPArray(param.MEPTB1.SPE.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTB1.QDP.BitField);
        CPArray(param.MEPTB1.QDP.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTB1.Interval.BitField);
        CPArray(param.MEPTB1.Interval.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTB1.Time.BitField);
        CPArray(param.MEPTB1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_EP_TC_1 :
      begin
        arrLen := Length(param.MEPTC1.OCI.BitField);
        CPArray(param.MEPTC1.OCI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTC1.QDP.BitField);
        CPArray(param.MEPTC1.QDP.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTC1.Interval.BitField);
        CPArray(param.MEPTC1.Interval.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTC1.Time.BitField);
        CPArray(param.MEPTC1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_PS_NA_1 :
      begin
        arrLen := Length(param.MPSNA1.SCD.BitField);
        CPArray(param.MPSNA1.SCD.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MPSNA1.QDS.BitField);
        CPArray(param.MPSNA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_ND_1 :
      begin
        arrLen := Length(param.MMEND1.NVA.BitField);
        CPArray(param.MMEND1.NVA.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_SP_TB_1 :
      begin
        arrLen := Length(param.MSPTB1.SIQ.BitField);
        CPArray(param.MSPTB1.SIQ.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MSPTB1.Time.BitField);
        CPArray(param.MPSNA1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_DP_TB_1 :
      begin
        arrLen := Length(param.MDPTB1.DIQ.BitField);
        CPArray(param.MDPTB1.DIQ.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MDPTB1.Time.BitField);
        CPArray(param.MDPTB1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ST_TB_1 :
      begin
        arrLen := Length(param.MSTTB1.VTI.BitField);
        CPArray(param.MSTTB1.VTI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MSTTB1.QDS.BitField);
        CPArray(param.MSTTB1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MSTTB1.Time.BitField);
        CPArray(param.MSTTB1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_BO_TB_1 :
      begin
        arrLen := Length(param.MBOTB1.BSI.BitField);
        CPArray(param.MBOTB1.BSI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MBOTB1.QDS.BitField);
        CPArray(param.MBOTB1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MBOTB1.Time.BitField);
        CPArray(param.MBOTB1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_TD_1 :
      begin
        arrLen := Length(param.MMETD1.NVA.BitField);
        CPArray(param.MMETD1.NVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETD1.QDS.BitField);
        CPArray(param.MMETD1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETD1.Time.BitField);
        CPArray(param.MMETD1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_TE_1 :
      begin
        arrLen := Length(param.MMETE1.SVA.BitField);
        CPArray(param.MMETE1.SVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETE1.QDS.BitField);
        CPArray(param.MMETE1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETE1.Time.BitField);
        CPArray(param.MMETE1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_ME_TF_1 :
      begin
        arrLen := Length(param.MMETF1.R32IEEE754.BitField);
        CPArray(param.MMETF1.R32IEEE754.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETF1.QDS.BitField);
        CPArray(param.MMETF1.QDS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MMETF1.Time.BitField);
        CPArray(param.MMETF1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_IT_TB_1 :
      begin
        arrLen := Length(param.MITTB1.BCR.BitField);
        CPArray(param.MITTB1.BCR.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MITTB1.Time.BitField);
        CPArray(param.MITTB1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_EP_TD_1 :
      begin
        arrLen := Length(param.MEPTD1.SEP.BitField);
        CPArray(param.MEPTD1.SEP.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTD1.Interval.BitField);
        CPArray(param.MEPTD1.Interval.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTD1.Time.BitField);
        CPArray(param.MEPTD1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_EP_TE_1 :
      begin
        arrLen := Length(param.MEPTE1.SPE.BitField);
        CPArray(param.MEPTE1.SPE.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTE1.QDP.BitField);
        CPArray(param.MEPTE1.QDP.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTE1.Interval.BitField);
        CPArray(param.MEPTE1.Interval.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTE1.Time.BitField);
        CPArray(param.MEPTE1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_EP_TF_1 :
      begin
        arrLen := Length(param.MEPTF1.OCI.BitField);
        CPArray(param.MEPTF1.OCI.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTF1.QDP.BitField);
        CPArray(param.MEPTF1.QDP.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTF1.Interval.BitField);
        CPArray(param.MEPTF1.Interval.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.MEPTF1.Time.BitField);
        CPArray(param.MEPTF1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_SC_NA_1 :
      begin
        arrLen := Length(param.CSCNA1.SCO.BitField);
        CPArray(param.CSCNA1.SCO.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_DC_NA_1 :
      begin
        arrLen := Length(param.CDCNA1.DCO.BitField);
        CPArray(param.CDCNA1.DCO.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_RC_NA_1 :
      begin
        arrLen := Length(param.CRCNA1.RCO.BitField);
        CPArray(param.CRCNA1.RCO.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_SE_NA_1 :
      begin
        arrLen := Length(param.CSENA1.NVA.BitField);
        CPArray(param.CSENA1.NVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.CSENA1.QOS.BitField);
        CPArray(param.CSENA1.QOS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_SE_NB_1 :
      begin
        arrLen := Length(param.CSENB1.SVA.BitField);
        CPArray(param.CSENB1.SVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.CSENB1.QOS.BitField);
        CPArray(param.CSENB1.QOS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_SE_NC_1 :
      begin
        arrLen := Length(param.CSENC1.R32IEEE754.BitField);
        CPArray(param.CSENC1.R32IEEE754.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.CSENC1.QOS.BitField);
        CPArray(param.CSENC1.QOS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_BO_NA_1 :
      begin
        arrLen := Length(param.CBONA1.BSI.BitField);
        CPArray(param.CBONA1.BSI.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.M_EI_NA_1 :
      begin
        arrLen := Length(param.MEINA1.COI.BitField);
        CPArray(param.MEINA1.COI.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_IC_NA_1 :
      begin
        arrLen := Length(param.CICNA1.QOI.BitField);
        CPArray(param.CICNA1.QOI.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_CI_NA_1 :
      begin
        arrLen := Length(param.CCINA1.QCC.BitField);
        CPArray(param.CCINA1.QCC.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_RD_NA_1 :
      begin
        /////////////////
      end;

      AsduTypes_E.C_CS_NA_1 :
      begin
        arrLen := Length(param.CCSNA1.Time.BitField);
        CPArray(param.CCSNA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_TS_NA_1 :
      begin
        arrLen := Length(param.CTSNA1.FBP.BitField);
        CPArray(param.CTSNA1.FBP.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_RP_NA_1 :
      begin
        arrLen := Length(param.CRPNA1.QRP.BitField);
        CPArray(param.CRPNA1.QRP.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.C_CD_NA_1 :
      begin
        arrLen := Length(param.CCDNA1.Interval.BitField);
        CPArray(param.CCDNA1.Interval.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.P_ME_NA_1 :
      begin
        arrLen := Length(param.PMENA1.NVA.BitField);
        CPArray(param.PMENA1.NVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.PMENA1.QPM.BitField);
        CPArray(param.PMENA1.QPM.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.P_ME_NB_1 :
      begin
        arrLen := Length(param.PMENB1.SVA.BitField);
        CPArray(param.PMENB1.SVA.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.PMENB1.QPM.BitField);
        CPArray(param.PMENB1.QPM.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.P_ME_NC_1 :
      begin
        arrLen := Length(param.PMENC1.R32IEEE754.BitField);
        CPArray(param.PMENC1.R32IEEE754.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.PMENC1.QPM.BitField);
        CPArray(param.PMENC1.QPM.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.P_AC_NA_1 :
      begin
        arrLen := Length(param.PACNA1.QPA.BitField);
        CPArray(param.PACNA1.QPA.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.F_FR_NA_1 :
      begin
        arrLen := Length(param.FFRNA1.NOF.BitField);
        CPArray(param.FFRNA1.NOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FFRNA1.LOF.BitField);
        CPArray(param.FFRNA1.LOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FFRNA1.FRQ.BitField);
        CPArray(param.FFRNA1.FRQ.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.F_SR_NA_1 :
      begin
        arrLen := Length(param.FSRNA1.NOF.BitField);
        CPArray(param.FSRNA1.NOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FSRNA1.NOS.BitField);
        CPArray(param.FSRNA1.NOS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FSRNA1.LOF.BitField);
        CPArray(param.FSRNA1.LOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FSRNA1.SRQ.BitField);
        CPArray(param.FSRNA1.SRQ.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.F_SC_NA_1 :
      begin
        arrLen := Length(param.FSCNA1.NOF.BitField);
        CPArray(param.FSCNA1.NOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FSCNA1.NOS.BitField);
        CPArray(param.FSCNA1.NOS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FSCNA1.SCQ.BitField);
        CPArray(param.FSCNA1.SCQ.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.F_LS_NA_1 :
      begin
        arrLen := Length(param.FLSNA1.NOF.BitField);
        CPArray(param.FLSNA1.NOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FLSNA1.NOS.BitField);
        CPArray(param.FLSNA1.NOS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FLSNA1.LSQ.BitField);
        CPArray(param.FLSNA1.LSQ.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FLSNA1.CHS.BitField);
        CPArray(param.FLSNA1.CHS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.F_AF_NA_1 :
      begin
        arrLen := Length(param.FAFNA1.NOF.BitField);
        CPArray(param.FAFNA1.NOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FAFNA1.NOS.BitField);
        CPArray(param.FAFNA1.NOS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FAFNA1.AFQ.BitField);
        CPArray(param.FAFNA1.AFQ.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.F_SG_NA_1 :
      begin
        arrLen := Length(param.FSGNA1.NOF.BitField);
        CPArray(param.FSGNA1.NOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FSGNA1.NOS.BitField);
        CPArray(param.FSGNA1.NOS.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FSGNA1.LOS.BitField);
        CPArray(param.FSGNA1.LOS.BitField, self.OI_Dat, arrLen, 0, offset);
      end;

      AsduTypes_E.F_DR_TA_1 :
      begin
        arrLen := Length(param.FDRTA1.NOF.BitField);
        CPArray(param.FDRTA1.NOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FDRTA1.LOF.BitField);
        CPArray(param.FDRTA1.LOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FDRTA1.SOF.BitField);
        CPArray(param.FDRTA1.SOF.BitField, self.OI_Dat, arrLen, 0, offset);

        offset := offset + arrLen;
        arrLen := Length(param.FDRTA1.Time.BitField);
        CPArray(param.FDRTA1.Time.BitField, self.OI_Dat, arrLen, 0, offset);
      end;
      else
      begin
        OI_Len := -1;
        Exit;
      end;
    end;
  end;

  procedure InfarmationObject_C.ParseArray(var descr : ApduDescription_R);
  var
    i, offset, itemLen : integer;
  begin
    itemLen := GetIOLen(descr.ASDUType);

    for i := 0 to descr.StrcQual.VSQ do
    begin
      offset                             := i * (itemLen + self.ObjectInformationAddressOctets);
      descr.InfObjDescr[i].InfObjAddress := ParseIOAddress(offset);

      ParseSingle(offset + self.ObjectInformationAddressOctets, descr.InfObjDescr[i], descr.ASDUType);
    end;
  end;

//******************************************************************************
//******************************************************************************
//******************************************************************************

  procedure InfarmationObject_C.AddIOAddress(InfObjAddress : Integer);
  var
    current : Byte;
  begin
      current   := InfObjAddress and $ff;
      AddByte(current);

      if ObjectInformationAddressOctets > 1 then
      begin
        current := (InfObjAddress and $ff00) shr 8;
        AddByte(current);
      end;

      if ObjectInformationAddressOctets > 2 then
      begin
        current := (InfObjAddress and $ff0000) shr 16;
        AddByte(current);
      end;
  end;

  function InfarmationObject_C.ParseIOAddress(offset : integer) : Integer;
  var
    address : Integer;
  begin
    address := OI_Dat[offset];

    if ObjectInformationAddressOctets > 1 then
      begin
        inc(offset);
        address := SetBit(address, OI_Dat[offset]  shl 8);
      end;

      if ObjectInformationAddressOctets > 2 then
      begin
        inc(offset);
        address := SetBit(address, OI_Dat[offset]  shl 16);
      end;

    Result := address;
  end;

  procedure InfarmationObject_C.AddByte(data : Byte);
  begin

    if (OI_Len + 1) >= Length(OI_Dat) then Exit;

    OI_Dat[OI_Len] := data;
    inc(OI_Len);
  end;

  procedure InfarmationObject_C.AddAnyBytes(data : Array of Byte);
  var
    i : Integer;
  begin
    for i := 0 to Length(data) - 1 do AddByte(data[i]);
  end;

  procedure InfarmationObject_C.ClearOI();
      var i : Integer;
  begin
    for i := 0 to INFORMATION_OBJECT_MAX_LEN do
    begin
      OI_Dat[i] := 0;
    end;
    OI_Len      := 0;
  end;

  function GetIOLen(typeAsdu : AsduTypes_E) : Integer;
  var
    iod : InfObjDescr_R;
  begin
    case typeAsdu  of
      AsduTypes_E.M_SP_NA_1 : Result := sizeof(iod.MSPNA1);
      AsduTypes_E.M_SP_TA_1 : Result := sizeof(iod.MSPTA1);
      AsduTypes_E.M_DP_NA_1 : Result := sizeof(iod.MDPNA1);
      AsduTypes_E.M_DP_TA_1 : Result := sizeof(iod.MDPTA1);
      AsduTypes_E.M_ST_NA_1 : Result := sizeof(iod.MSTNA1);
      AsduTypes_E.M_ST_TA_1 : Result := sizeof(iod.MSTTA1);
      AsduTypes_E.M_BO_NA_1 : Result := sizeof(iod.MBONA1);
      AsduTypes_E.M_BO_TA_1 : Result := sizeof(iod.MBOTA1);
      AsduTypes_E.M_ME_NA_1 : Result := sizeof(iod.MMENA1);
      AsduTypes_E.M_ME_TA_1 : Result := sizeof(iod.MMETA1);
      AsduTypes_E.M_ME_NB_1 : Result := sizeof(iod.MMENB1);
      AsduTypes_E.M_ME_TB_1 : Result := sizeof(iod.MMETB1);
      AsduTypes_E.M_ME_NC_1 : Result := sizeof(iod.MMENC1);
      AsduTypes_E.M_ME_TC_1 : Result := sizeof(iod.MMETC1);
      AsduTypes_E.M_IT_NA_1 : Result := sizeof(iod.MITNA1);
      AsduTypes_E.M_IT_TA_1 : Result := sizeof(iod.MITTA1);
      AsduTypes_E.M_EP_TA_1 : Result := sizeof(iod.MEPTA1);
      AsduTypes_E.M_EP_TB_1 : Result := sizeof(iod.MEPTB1);
      AsduTypes_E.M_EP_TC_1 : Result := sizeof(iod.MEPTC1);
      AsduTypes_E.M_PS_NA_1 : Result := sizeof(iod.MPSNA1);
      AsduTypes_E.M_ME_ND_1 : Result := sizeof(iod.MMEND1);
      AsduTypes_E.M_SP_TB_1 : Result := sizeof(iod.MSPTB1);
      AsduTypes_E.M_DP_TB_1 : Result := sizeof(iod.MDPTB1);
      AsduTypes_E.M_ST_TB_1 : Result := sizeof(iod.MSTTB1);
      AsduTypes_E.M_BO_TB_1 : Result := sizeof(iod.MBOTB1);
      AsduTypes_E.M_ME_TD_1 : Result := sizeof(iod.MMETD1);
      AsduTypes_E.M_ME_TE_1 : Result := sizeof(iod.MMETE1);
      AsduTypes_E.M_ME_TF_1 : Result := sizeof(iod.MMETF1);
      AsduTypes_E.M_IT_TB_1 : Result := sizeof(iod.MITTB1);
      AsduTypes_E.M_EP_TD_1 : Result := sizeof(iod.MEPTD1);
      AsduTypes_E.M_EP_TE_1 : Result := sizeof(iod.MEPTE1);
      AsduTypes_E.M_EP_TF_1 : Result := sizeof(iod.MEPTF1);
      AsduTypes_E.C_SC_NA_1 : Result := sizeof(iod.CSCNA1);
      AsduTypes_E.C_DC_NA_1 : Result := sizeof(iod.CDCNA1);
      AsduTypes_E.C_RC_NA_1 : Result := sizeof(iod.CRCNA1);
      AsduTypes_E.C_SE_NA_1 : Result := sizeof(iod.CSENA1);
      AsduTypes_E.C_SE_NB_1 : Result := sizeof(iod.CSENB1);
      AsduTypes_E.C_SE_NC_1 : Result := sizeof(iod.CSENC1);
      AsduTypes_E.C_BO_NA_1 : Result := sizeof(iod.CBONA1);
      AsduTypes_E.M_EI_NA_1 : Result := sizeof(iod.MEINA1);
      AsduTypes_E.C_IC_NA_1 : Result := sizeof(iod.CICNA1);
      AsduTypes_E.C_CI_NA_1 : Result := sizeof(iod.CCINA1);
      AsduTypes_E.C_RD_NA_1 : Result := sizeof(iod.CRDNA1);
      AsduTypes_E.C_CS_NA_1 : Result := sizeof(iod.CCSNA1);
      AsduTypes_E.C_TS_NA_1 : Result := sizeof(iod.CTSNA1);
      AsduTypes_E.C_RP_NA_1 : Result := sizeof(iod.CRPNA1);
      AsduTypes_E.C_CD_NA_1 : Result := sizeof(iod.CCDNA1);
      AsduTypes_E.P_ME_NA_1 : Result := sizeof(iod.PMENA1);
      AsduTypes_E.P_ME_NB_1 : Result := sizeof(iod.PMENB1);
      AsduTypes_E.P_ME_NC_1 : Result := sizeof(iod.PMENC1);
      AsduTypes_E.P_AC_NA_1 : Result := sizeof(iod.PACNA1);
      AsduTypes_E.F_FR_NA_1 : Result := sizeof(iod.FFRNA1);
      AsduTypes_E.F_SR_NA_1 : Result := sizeof(iod.FSRNA1);
      AsduTypes_E.F_SC_NA_1 : Result := sizeof(iod.FSCNA1);
      AsduTypes_E.F_LS_NA_1 : Result := sizeof(iod.FLSNA1);
      AsduTypes_E.F_AF_NA_1 : Result := sizeof(iod.FAFNA1);
      AsduTypes_E.F_SG_NA_1 : Result := sizeof(iod.FSGNA1);
      AsduTypes_E.F_DR_TA_1 : Result := sizeof(iod.FDRTA1);
      else Result := -1;
    end;
  end;
end.

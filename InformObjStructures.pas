unit InformObjStructures;

interface
uses InformObjFields;

type
  M_SP_NA_1_R = record
    SIQ : SIQ_R;
  end;

//******************************************************************************

  M_SP_TA_1_R = record
    SIQ  : SIQ_R;
    Time : Time3_R;
  end;

//******************************************************************************

  M_DP_NA_1_R = record
    DIQ : DIQ_R;
  end;

//******************************************************************************

  M_DP_TA_1_R = record
    DIQ  : DIQ_R;
    Time : Time3_R;
  end;

//******************************************************************************

  M_ST_NA_1_R = record
    VTI : VTI_R;
    QDS : QDS_R;
  end;

//******************************************************************************

  M_ST_TA_1_R = record
    VTI  : VTI_R;
    QDS  : QDS_R;
    Time : Time3_R;
  end;

//******************************************************************************

  M_BO_NA_1_R = record
    BSI : BSI_R;
    QDS : QDS_R;
  end;

//******************************************************************************

  M_BO_TA_1_R = record
    BSI  : BSI_R;
    QDS  : QDS_R;
    Time : Time3_R;
  end;

//******************************************************************************

  M_ME_NA_1_R = record
    NVA : NVA_R;
    QDS : QDS_R;
  end;

//******************************************************************************

  M_ME_TA_1_R = record
    NVA  : NVA_R;
    QDS  : QDS_R;
    Time : Time3_R;
  end;

//******************************************************************************

  M_ME_NB_1_R = record
    SVA : SVA_R;
    QDS : QDS_R;
  end;

//******************************************************************************

  M_ME_TB_1_R = record
    SVA  : SVA_R;
    QDS  : QDS_R;
    Time : Time3_R;
  end;

//******************************************************************************

  M_ME_NC_1_R = record
    R32IEEE754 : R32IEEE754_R;
    QDS        : QDS_R;
  end;

//******************************************************************************

  M_ME_TC_1_R = record
    R32IEEE754 : R32IEEE754_R;
    QDS        : QDS_R;
    Time       : Time3_R;
  end;

//******************************************************************************

  M_IT_NA_1_R = record
    BCR : BCR_R;
  end;

//******************************************************************************

  M_IT_TA_1_R = record
    BCR  : BCR_R;
    Time : Time3_R;
  end;

//******************************************************************************

  M_EP_TA_1_R = record
    SEP      : SEP_R;
    Interval : Time2_R;
    Time     : Time3_R;
  end;

//******************************************************************************

  M_EP_TB_1_R = record
    SPE      : SPE_R;
    QDP      : QDP_R;
    Interval : Time2_R;
    Time     : Time3_R;
  end;

//******************************************************************************

  M_EP_TC_1_R = record
    OCI : OCI_R;
    QDP : QDP_R;
    Interval : Time2_R;
    Time     : Time3_R;
  end;

//******************************************************************************

  M_PS_NA_1_R = record
    SCD : SCD_R;
    QDS : QDS_R;
  end;

//******************************************************************************

  M_ME_ND_1_R = record
    NVA : NVA_R;
  end;

//******************************************************************************

  M_SP_TB_1_R = record
    SIQ   : SIQ_R;
    Time  : Time7_R;
  end;

//******************************************************************************

  M_DP_TB_1_R = record
    DIQ   : DIQ_R;
    Time  : Time7_R;
  end;

//******************************************************************************

  M_ST_TB_1_R = record
    VTI  : VTI_R;
    QDS  : QDS_R;
    Time : Time7_R;
  end;

//******************************************************************************

  M_BO_TB_1_R = record
    BSI  : BSI_R;
    QDS  : QDS_R;
    Time : Time7_R;
  end;

//******************************************************************************

  M_ME_TD_1_R = record
    NVA  : NVA_R;
    QDS  : QDS_R;
    Time : Time7_R;
  end;

//******************************************************************************

  M_ME_TE_1_R = record
    SVA  : SVA_R;
    QDS  : QDS_R;
    Time : Time7_R;
  end;

//******************************************************************************

  M_ME_TF_1_R = record
    R32IEEE754 : R32IEEE754_R;
    QDS        : QDS_R;
    Time       : Time7_R;
  end;

//******************************************************************************

  M_IT_TB_1_R = record
    BCR  : BCR_R;
    Time : Time7_R;
  end;

//******************************************************************************

  M_EP_TD_1_R = record
    SEP      : SEP_R;
    Interval : Time2_R;
    Time     : Time7_R;
  end;

//******************************************************************************

  M_EP_TE_1_R = record
    SPE      : SPE_R;
    QDP      : QDP_R;
    Interval : Time2_R;
    Time     : Time7_R;
  end;

//******************************************************************************

  M_EP_TF_1_R = record
    OCI : OCI_R;
    QDP : QDP_R;
    Interval : Time2_R;
    Time     : Time7_R;
  end;

//******************************************************************************

  C_SC_NA_1_R = record
    SCO : SCO_R;
  end;

//******************************************************************************

  C_DC_NA_1_R = record
    DCO : DCO_R;
  end;

//******************************************************************************

  C_RC_NA_1_R = record
    RCO : RCO_R;
  end;

//******************************************************************************

  C_SE_NA_1_R = record
    NVA : NVA_R;
    QOS : QOS_R;
  end;

//******************************************************************************

  C_SE_NB_1_R = record
    SVA  : SVA_R;
    QOS : QOS_R;
  end;

//******************************************************************************

  C_SE_NC_1_R = record
    R32IEEE754 : R32IEEE754_R;
    QOS        : QOS_R;
  end;

//******************************************************************************

  C_BO_NA_1_R = record
    BSI : BSI_R;
  end;

//******************************************************************************

  M_EI_NA_1_R = record
    COI : COI_R;
  end;

//******************************************************************************

  C_IC_NA_1_R = record
    QOI : QOI_R;
  end;

//******************************************************************************

  C_CI_NA_1_R = record
    QCC : QCC_R;
  end;

//******************************************************************************

  C_RD_NA_1_R = record
  end;

//******************************************************************************

  C_CS_NA_1_R = record
    Time  : Time7_R;
  end;

//******************************************************************************

  C_TS_NA_1_R = record
    FBP : FBP_R;
  end;

//******************************************************************************

  C_RP_NA_1_R = record
    QRP : QRP_R;
  end;

//******************************************************************************

  C_CD_NA_1_R = record
    Interval : Time2_R;
  end;

//******************************************************************************

  P_ME_NA_1_R = record
    NVA : NVA_R;
    QPM : QPM_R;
  end;

//******************************************************************************

  P_ME_NB_1_R = record
    SVA  : SVA_R;
    QPM : QPM_R;
  end;

//******************************************************************************

  P_ME_NC_1_R = record
    R32IEEE754 : R32IEEE754_R;
    QPM        : QPM_R;
  end;

//******************************************************************************

  P_AC_NA_1_R = record
    QPA : QPA_R;
  end;

//******************************************************************************

  F_FR_NA_1_R = record
    NOF : NOF_R;
    LOF : LOF_R;
    FRQ : FRQ_R;
  end;

//******************************************************************************

  F_SR_NA_1_R = record
    NOF : NOF_R;
    NOS : NOS_R;
    LOF : LOF_R;
    SRQ : SRQ_R;
  end;

//******************************************************************************

  F_SC_NA_1_R = record
    NOF : NOF_R;
    NOS : NOS_R;
    SCQ : SCQ_R;
  end;

//******************************************************************************

  F_LS_NA_1_R = record
    NOF : NOF_R;
    NOS : NOS_R;
    LSQ : LSQ_R;
    CHS : CHS_R;
  end;

//******************************************************************************

  F_AF_NA_1_R = record
    NOF : NOF_R;
    NOS : NOS_R;
    AFQ : AFQ_R;
  end;

//******************************************************************************

  F_SG_NA_1_R = record
    NOF : NOF_R;
    NOS : NOS_R;
    LOS : LOS_R;
  end;

//******************************************************************************

  F_DR_TA_1_R = record
    NOF   : NOF_R;
    LOF   : LOF_R;
    SOF   : SOF_R;
    Time  : Time7_R;
  end;

implementation



end.
